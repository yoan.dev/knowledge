#-*- coding: utf-8 -*-
import matplotlib.pyplot as plt
from package.data import *

class BaseGraph:

    def __init__(self):
        self.title = "Your graph title"
        self.x_label = "X-axis label"
        self.y_label = "X-axis label"
        self.show_grid = True

    def show(self, format):
        # x_values = gather only x_values from our zones
        # y_values = gather only y_values from our zones
        x_values, y_values = self.xy_values()
        if format != 'bar':
            plt.plot(x_values, y_values, 'b')
        else:
            plt.bar(x_values, y_values, color = 'red')
            plt.xticks(x_values, rotation = 90)
            plt.subplots_adjust(left=0.1, bottom=0.25, right=0.95, top=0.95, wspace=None, hspace=None)
        plt.xlabel(self.x_label)
        plt.ylabel(self.y_label)
        plt.title(self.title)
        plt.grid(self.show_grid)
        plt.show()

    def xy_values(self):
        raise NotImplementedError

class WinrateGraph(BaseGraph):

    def __init__(self, name):
        super().__init__()
        self.name = name
        self.title = "Winrate by session"
        self.x_label = "Total {} sessions".format(name)
        self.y_label = "Winrate in %"

    def xy_values(self):
        x_values, y_values = get_list_winrate(self.name)
        return x_values, y_values


class MethodsGraph(BaseGraph):

    def __init__(self, name):
        super().__init__()
        self.name = name
        self.title = "Percentage of times the method is found."
        self.x_label = "All methods of {}".format(name)
        self.y_label = "method found %"

    def xy_values(self):
        x_values, y_values = get_list_method(self.name)
        return x_values, y_values

class TriesByMethodGraph(BaseGraph):

    def __init__(self, name):
        super().__init__()
        self.name = name
        self.title = "Average of tries by method found."
        self.x_label = "Total {} sessions".format(name)
        self.y_label = "tries by method"

    def xy_values(self):
        x_values, y_values = get_list_tries_method(self.name)
        return x_values, y_values

class SessionByDayGraph(BaseGraph):

    def __init__(self, name):
        super().__init__()
        self.name = name
        self.title = "Sessions per day"
        self.x_label = "Day".format(name)
        self.y_label = "Number sessions"

    def xy_values(self):
        x_values, y_values = get_list_session_day(self.name)
        return x_values, y_values

class ExerciseWinrateGraph(BaseGraph):
    def __init__(self, name):
        super().__init__()
        self.name = name
        self.title = "puzzle solve by session"
        self.x_label = "Total {} sessions".format(name)
        self.y_label = "puzzle solve"

    def xy_values(self):
        x_values, y_values = get_list_puzzle_solve(self.name)
        return x_values, y_values

class ExerciseDayGraph(BaseGraph):

    def __init__(self, name):
        super().__init__()
        self.name = name
        self.title = "Sessions per day"
        self.x_label = "Day".format(name)
        self.y_label = "Number sessions"

    def xy_values(self):
        x_values, y_values = get_list_exercise_session_day(self.name)
        return x_values, y_values