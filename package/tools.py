import os
from donnees import *
from random import *
#
#
##################################################################################################
##### FUNCTIONS CALLED IN THIS FILE: #############################################################
##################################################################################################
#
##
###
def simplify_methods(type_obj):
  """Function take the list of all methods, but hide the parameters of all methods for showing (...),
   thus we have a better view into the rest of methods to find."""

  methods = []

  for i in range(0, len(TYPE_OF[type_obj]), 1):
    method = TYPE_OF[type_obj][i]
    end_method = method.find('(')

    if method[end_method+1:end_method+2] == ')':
      param = '()'
    else:
      param = '(...)'

    method = method[:end_method]+param
    methods.append(method)

  return methods
###
##
#
#
##
###
def simplify_list_methods(methods):
  """Function take the list of all methods, but hide the parameters of all methods for showing (...),
   thus we have a better view into the rest of methods to find."""

  simplify_methods = []

  for method in methods:
    end_method = method.find('(')

    method = method[:end_method]

    if len(method) > 12:
      method = method[:9]+'...'

    simplify_methods.append(method)

  return simplify_methods
###
##
#
#
##
###
def get_longest_item(list):
  """Functions see in all items of the list, which one is the longest. item is type(str)."""

  longest = ''
  for item in list:

    if len(longest) < len(item):
      longest = item

  return len(longest)
###
##
#
#
##
###
def reverse_binary_list(list):
  """"Return a list like [0,0,1,1,0,1] ==> [1,1,0,0,1,0]"""

  results = []

  for i in list:

    if i:
      results.append(0)
    else:
      results.append(1)

  return results
###
##
#
#
##
###
def replace_zero_by_escape(str):
  list = [char for char in str]
  for i, j in enumerate(list):

    if j != '0':
      break
    
    list[i] = ' '
  list = ''.join(list)
  return list
###
##
#
##
###
def get_10_basic_puzzles(type_obj, length):
  
  methods, name_puzzles, puzzles = [], [], []

  for method in BASIC[type_obj].keys():
    methods.append(method)
  
  #we want to retrieve length random name

  while len(name_puzzles) < length:
    index_random = randint(0, len(methods)-1)
    if methods[index_random] not in name_puzzles:
      name_puzzles.append(methods[index_random])
  
  for method in name_puzzles:
    rand_numb = randint(0,1)
    puzzles.append(BASIC[type_obj][method][rand_numb])

  return puzzles

def get_10_expert_puzzles():
  methods, name_puzzles, puzzles = [], [], []

  
  
  
  
