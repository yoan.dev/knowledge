import os
from donnees import *
from donnees_library import *
from random import *
from package.tools import *
from package.data import *
#
###########################################################################
############################# M E N U #####################################
###########################################################################
#
def print_menu():
  """---------Menu---------
      1 - Find methods
      2- Exercise
      3 - overview of sessions
      4 - see graph
      5 - Library
  """
  os.system('cls' if os.name == 'nt' else 'clear')
  print("KNOWLEDGE".center(120,'-'))
  print("\n")
  string_menu = "------------ Menu ------------"
  string_foot_menu = "-"*len(string_menu)
  print(string_menu.center(120))
  print("\n")
  print(("1 - Find methods".rjust(66))) 
  print("\n")
  print("2 - Exercise".rjust(62))
  print("\n")
  print("3 - overview of sessions".rjust(74)) 
  print("\n")
  print("4 - see graph".rjust(63))
  print("\n")
  print("5 - Library".rjust(61))
  print("\n")
  print(string_foot_menu.center(120))
  print("\n\n")
  print("[Q] --> quit".center(120))
  print("\n")
##
##
###########################################################################
########################### FIND METHOD ###################################
###########################################################################
#
def print_menu_method():
  """---------Find methods of---------
      1 - str
      2- list
      ...
      7 - native fc
  """
  os.system('cls' if os.name == 'nt' else 'clear')
  print("KNOWLEDGE".center(120,'-'))
  print("\n\n")
  string_menu_method = "----- Find methods of -----"
  string_menu_method_foot = "-"*len(string_menu_method)
  print(string_menu_method.center(120))
  print("\n")
  for i in range(0, len(OBJECTS)):
    string = "{} - {}".format((i+1), OBJECTS[i]).ljust(13)
    print(string.center(120))

  print("\n")
  print(string_menu_method_foot.center(120))
  print("\n\n\n")
  print(" [Q] --> quit| [B] --> back | [R] --> random".center(120))
  print("\n")
##
##
def print_method_end(methods, time, type_obj, total_try):
  """Display the result of the session: duration, winrate, all methods not find."""

  os.system('cls' if os.name == 'nt' else 'clear')
  methods_find, methods_not_find = [], []

  for key, method_find in enumerate(methods):

    if method_find:
      methods_find.append(TYPE_OF[type_obj][key])
    else:
      methods_not_find.append(TYPE_OF[type_obj][key])
  
  time = str(time)
  winrate = round((len(methods_find) / len(TYPE_OF[type_obj]) * 100))
  n_methods_find = len(methods_find)
  print("KNOWLEDGE".center(120,'-'))
  print("\n")
  if not methods_find:
    print("this session will not be saved, you have to find a method to save it.".center(120))
  else:
    print("The session lasted {}min. You find {}/{}, so {}% winrate with {} tries."
  .format(time, n_methods_find, len(TYPE_OF[type_obj]), winrate, total_try).center(120))

  if not methods_not_find:
    print("\n")
    print("You find all the methods !".center(120))
  else:
      print("\n")
      print("All methods you don't know!".center(120))

  print("\n")
  
  print_methods(methods_not_find, reverse_binary_list(methods), True)

  return [time, winrate, total_try, methods_find, methods_not_find]
##
##
#----------------------------------------------------------------------------
#--########################################################################--
#--############################# EXERCISE #################################--
#--########################################################################--
#----------------------------------------------------------------------------
#
def print_menu_exercise():
  """-----Exercise-----
        1 - str
        2 - list
        ...
        8 - 
  """
  os.system('cls' if os.name == 'nt' else 'clear')
  print("KNOWLEDGE".center(120,'-'))
  print("\n\n")
  string_menu_exercise = "----- Exercise -----"
  string_menu_exercise_foot = "-"*len(string_menu_exercise)
  print(string_menu_exercise.center(120))
  print("\n")
  print("1 - str".rjust(60))
  print("2 - list".rjust(61))
  print("3 - dict".rjust(61))
  print("4 - set".rjust(60))
  print("5 - tuple".rjust(62))
  print("6 - file".rjust(61))
  print("7 - native fc".rjust(66))
  print("8 - Expert (soon)".rjust(70))
  print("\n")
  print(string_menu_exercise_foot.center(120))
  print("\n\n\n")
  print("[Q] --> quit | [B] --> back".center(120))
  print("\n")
##
##
#------------------------------------------------
#------------- EXERCISE BASIC -------------------
#------------------------------------------------
#
def print_menu_exercise_basic():
  """ - - - exercise basic - - - 
            1 - str
            2 - list
            ....
            7 - native fc
  """
  os.system('cls' if os.name == 'nt' else 'clear')
  print("KNOWLEDGE".center(120,'-'))
  print("\n\n")
  string_menu_exercise_basic = "----- Exercise Basic -----"
  string_menu_exercise_basic_foot = "-"*len(string_menu_exercise_basic)
  print(string_menu_exercise_basic.center(120))
  print("\n")
  for i in range(0, len(OBJECTS)):
    string = "{} - {}".format((i+1), OBJECTS[i]).ljust(13)
    print(string.center(120))
  print("\n")
  print(string_menu_exercise_basic_foot.center(120))
  print("\n\n\n")
  print("[Q] --> quit | [B] --> back".center(120))
  print("\n")
##
##
def print_exercise_basic(puzzle, resolve, type_obj, tries, duration, print_hour):
  """
    --- 14h21 --> 1min tries 5 - Exercise basic of {} solved - [x] of [10] ---
    Question about the current type_obj
  """
  os.system('cls' if os.name == 'nt' else 'clear')
  if type_obj == 'tuple':
    lenght_puzzle = 2
  else:
    lenght_puzzle = 10
  print("KNOWLEDGE".center(120,'-'))
  print("\n\n")
  string_exercise_basic = "----- {} --> {}min - tries: {} - Exercise Basic of {} solved - [{}] of [{}]-----".format(print_hour, duration,
  tries, type_obj.upper(), resolve, lenght_puzzle)
  string_exercise_basic_foot = "-" * len(string_exercise_basic)
  print(string_exercise_basic.center(120))
  print("\n")
  for i in puzzle['question']:
    print("                        "+i)
  
  print("\n")
  print(string_exercise_basic_foot.center(120))
  print("\n\n\n")
  print("[Q] --> quit | [E] --> end | [N] --> next".center(120))
  print("\n")

###
##
def print_exercise_basic_end(duration, resolve, total_try, type_obj):
  """The session on STR lasted 1min. You solved 2/10 puzzles in 3 tries. ---
    [L] --> See the specific lesson for the unsolved puzzle
  """
  os.system('cls' if os.name == 'nt' else 'clear')
  print("KNOWLEDGE".center(120,'-'))
  print("\n\n")
  if type_obj == 'tuple':
    lenght_puzzle = 2
  else:
    lenght_puzzle = 10
  if not resolve:
    print("This session will not ne saved, you have to solve a puzzle to save it.".center(120))
  else:
    string_exercise_basic_end = "The session on {} lasted {}min. You solved {}/{} puzzles in {} tries.".format(type_obj.upper(),
    duration,  resolve, lenght_puzzle, total_try).center(120)
    string_exercise_basic_end_foot = "-" * len(string_exercise_basic_end)
    print(string_exercise_basic_end.center(120))
  print("\n")
  print("\n")

  if (resolve != 10 and lenght_puzzle == 10) or (resolve != 2 and lenght_puzzle == 2):
    print(" [L] --> See the specific lesson for the unsolved puzzle".center(120))
    print("\n")
  
  print("[Q] --> quit | [M] --> menu".center(120))
  print("\n")
##
##
#------------------------------------------------
#------------- EXERCISE BASIC -------------------
#------------------------------------------------
#
def print_exercise_expert(puzzle, resolve, tries, duration, print_hour):
  """
    --- 14h21 --> 1min tries 5 - Exercise expert  solved - [x] of [10] ---
    Question about the current type_obj
  """
  os.system('cls' if os.name == 'nt' else 'clear')

  print("KNOWLEDGE".center(120,'-'))
  print("\n\n")
  string_exercise_expert = "----- {} --> {}min - tries: {} - Exercise expert  solved - [{}] of [10]-----".format(print_hour, duration,
  tries, resolve)
  string_exercise_expert_foot = "-" * len(string_exercise_expert)
  print(string_exercise_expert.center(120))
  print("\n")
  for i in puzzle['question']:
    print("                        "+i)
  
  print("\n")
  print(string_exercise_expert_foot.center(120))
  print("\n\n\n")
  print("[Q] --> quit | [E] --> end | [N] --> next".center(120))
  print("\n")
##
##
def print_exercise_expert_end(duration, resolve, total_try):
  """The session on STR lasted 1min. You solved 2/10 puzzles in 3 tries. ---
    
  """
  os.system('cls' if os.name == 'nt' else 'clear')
  print("KNOWLEDGE".center(120,'-'))
  print("\n\n")

  if not resolve:
    print("This session will not ne saved, you have to solve a puzzle to save it.".center(120))
  else:
    string_exercise_expert_end = "The expert session lasted {}min. You solved {}/10 puzzles in {} tries.".format(duration, resolve, total_try).center(120)
    string_exercise_expert_end_foot = "-" * len(string_exercise_expert_end)
    print(string_exercise_expert_end.center(120))
  print("\n")
  print("\n")


  
  print("[Q] --> quit | [M] --> menu".center(120))
  print("\n")
#----------------------------------------------------------------------------
#--########################################################################--
#--############################# OVERVIEW #################################--
#--########################################################################--
#----------------------------------------------------------------------------
def print_menu_overview():
  """------ Overview of ------
      1 - Find methods      12
      2 - Exercise basic    3
      3 - Exercise expert   42
  """
  os.system('cls' if os.name == 'nt' else 'clear')
  count_sessions_method = sum(get_data_list_total_sessions_methods())
  count_sessions_method = replace_zero_by_escape(str(count_sessions_method).zfill(3))
  count_sessions_exercise_basic = sum(get_data_list_total_sessions_exercise_basic())
  count_sessions_exercise_basic = replace_zero_by_escape(str(count_sessions_exercise_basic).zfill(3))
  count_sessions_exercise_expert = sum(get_data_list_total_sessions_exercise_basic())
  count_sessions_exercise_expert = replace_zero_by_escape(str(count_sessions_exercise_basic).zfill(3))
  print("KNOWLEDGE".center(120,'-'))
  print("\n")
  string_menu = "--------------- Overview of ---------------"
  string_foot_menu = "-"*len(string_menu)
  print(string_menu.center(120))
  print("\n")
  string_1 = "1 - Find methods".ljust(20)
  string_1 = string_1+'{}'.format(count_sessions_method)
  string_2 = "2 - Exercise basic".ljust(20)
  string_2 = string_2+'{}'.format(count_sessions_exercise_basic)
  string_3 = "3 - Exercise expert".ljust(20)
  string_3 = string_3+'{}'.format("(soon)")
  print(string_1.center(120)) 
  print(string_2.center(120))
  print(string_3.center(122)) 
  print("\n")
  print(string_foot_menu.center(120))
  print("\n\n\n")
  print("[Q] --> quit | [B] --> back".center(120))
  print("\n")

#------------------------------------------------
#--------------- FIND METHOD --------------------
#------------------------------------------------
#
def print_menu_overview_method():
  """- - - overview find methods - - - 
            1 - str 
            2 - list 
            ... 
            7 - native fc"""

  os.system('cls' if os.name == 'nt' else 'clear')
  list_total_sessions_method = get_data_list_total_sessions_methods()
  print("KNOWLEDGE".center(120,'-'))
  print("\n")
  string_menu_overview = "-------- Overview find methods --------"
  string_menu_overview_foot = "-"*len(string_menu_overview)
  print(string_menu_overview.center(120))
  print("\n")
  for i in range(0, len(OBJECTS)):
    n_sessions = replace_zero_by_escape(str(list_total_sessions_method[i]).zfill(3))
    string = "{} - {}".format((i+1), OBJECTS[i]).ljust(17)
    string = string+'{}'.format(n_sessions)
    print(string.center(120))

  print("\n")
  print(string_menu_overview_foot.center(120))
  print("\n\n\n")
  print("[Q] --> quit | [B] --> back".center(120))
  print("\n")
##
##
def print_start_session_overview_method_header(type_obj, count_page, max_page):
  """----------- All sessions of type_obj page [x] of [y] ---------------
  """
  os.system('cls' if os.name == 'nt' else 'clear')
  print("KNOWLEDGE".center(120,'-'))
  print("\n")
  string_overview = "------------- All sessions of {}  page [{}] of [{}] -------------".format(type_obj, count_page, max_page)
  string_overview_foot = "-"*len(string_overview)
  print(string_overview.center(120))
  print("\n")
  return string_overview_foot
##
##
def print_start_session_overview_methods(page, length_page, count_page, string_foot):
  """
  |   1 -- 24-02-2022 11:55 --   7% ||  11 -- 24-02-2022 14:26 --   3% |
  |   2 -- 24-02-2022 13:39 --  13% ||  12 -- 24-02-2022 14:34 --   3% |
  """
  if not page:
    print("No session exists.".center(120))
    print("\n")
    print(string_foot.center(120))
    print("\n")
    print("\n")
    print("[Q] --> quit | [B] --> back | [>] --> next page | [<] previous page".center(120))
    print("\n")

  else:
    if length_page < 11:

      for i in range(0, length_page, 1):
        number_session = replace_zero_by_escape(str((count_page*20+i+1)).zfill(3))
        winrate = replace_zero_by_escape(str(page[i]['winrate']).zfill(3))
        print_session = "| {} -- {} -- {}% |".format(number_session, page[i]['date'], winrate).center(120)
        print(print_session)
    else:

      for i in range(0, 10, 1):
        number_session_left = replace_zero_by_escape(str((count_page*20) + i+1).zfill(3))
        number_session_right = replace_zero_by_escape(str((1*count_page*20) + i+11).zfill(3))

        if i+10 < length_page:
          winrate = replace_zero_by_escape(str(page[i]['winrate']).zfill(3))
          winrate_10 = replace_zero_by_escape(str(page[i+10]['winrate']).zfill(3))
          print_session = "| {} -- {} -- {}% || {} -- {} -- {}% |".format(number_session_left,
          page[i]['date'], winrate, number_session_right, page[i+10]['date'], winrate_10).center(120)
          print(print_session)
        else:
          winrate = replace_zero_by_escape(str(page[i]['winrate']).zfill(3))
          print_session = "| {} -- {} -- {}% |".format(number_session_left, page[i]['date'], winrate).rjust(60)
          print(print_session)

    print("\n")
    print(string_foot.center(120))
    print("\n")
    print("\n")
    print("[{}-{}] more about session | [Q] --> quit | [B] --> back | [>] --> next page | [<] --> previous page".format(count_page*20+1,
    count_page*20+length_page).center(120))
    print("\n")
##
##
def print_session_overview_method_focus(choice, type_obj, date, time, winrate, total_try, find , not_find):
  """Session 13 on the str object on 24-02-2020 11:55. Duration 1min for a winrate of 7%.
                          - - Methods find - -
                          lower() | upper ()
                                ....
  """
  os.system('cls' if os.name == 'nt' else 'clear')
  
  print("KNOWLEDGE".center(120,'-'))
  print("\n")
  print("Session {} on the {} object on {}. Duration {}min for a winrate of {}%.".format(choice, type_obj, date, time, winrate).center(120))
  if len(find) != 0:
    average_method_try = total_try / len(find)
  else:
    average_method_try = 0
  print("Average by method: {:.2f} tries | total tries: {}.".format(average_method_try, total_try).center(120))
  print("\n")
  print("- - Methods find - -".center(120))
  print_methods(find, [], True)
  print("- - Methods not find - -".center(120))
  print_methods(not_find, [], True)
#------------------------------------------------
#-------------- EXERCISE BASIC ------------------
#------------------------------------------------
#
def print_menu_overview_exercise_basic():
  """- - - overview exercise basic - - - 
            1 - str 
            2 - list 
            ... 
            7 - native fc"""
  os.system('cls' if os.name == 'nt' else 'clear')
  #get good sessions
  list_total_sessions_method = get_data_list_total_sessions_exercise_basic()
  print("KNOWLEDGE".center(120,'-'))
  print("\n")
  string_menu_overview = "-------- Overview exercise basic --------"
  string_menu_overview_foot = "-"*len(string_menu_overview)
  print(string_menu_overview.center(120))
  print("\n")
  for i in range(0, len(OBJECTS)):
    n_sessions = replace_zero_by_escape(str(list_total_sessions_method[i]).zfill(3))
    string = "{} - {}".format((i+1), OBJECTS[i]).ljust(17)
    string = string+'{}'.format(n_sessions)
    print(string.center(120))

  print("\n")
  print(string_menu_overview_foot.center(120))
  print("\n\n\n")
  print("[Q] --> quit | [B] --> back".center(120))
  print("\n")
##
##
def print_start_session_overview_exercise_basic_header(type_obj, count_page, max_page):
  """----------- All sessions exercise basic of type_obj page [x] of [y] ---------------"""

  os.system('cls' if os.name == 'nt' else 'clear')
  print("KNOWLEDGE".center(120,'-'))
  print("\n")
  string_overview = "------------- All sessions exercise basic of {}  page [{}] of [{}] -------------".format(type_obj, count_page, max_page)
  string_overview_foot = "-"*len(string_overview)
  print(string_overview.center(120))
  print("\n")
  return string_overview_foot
##
##
def print_start_session_overview_exercise_basic(page, length_page, count_page, string_foot, type_obj):
  """
  |   1 -- 24-02-2022 11:55 --   70% ||  11 -- 24-02-2022 14:26 --   30% |
  |   2 -- 24-02-2022 13:39 --   10% ||  12 -- 24-02-2022 14:34 --   50% |
  """
  if type_obj == 'tuple':
    ratio = 50
  else:
    ratio = 10
  if not page:
    print("No session exists.".center(120))
    print("\n")
    print(string_foot.center(120))
    print("\n")
    print("\n")
    print("[Q] --> quit | [B] --> back | [>] --> next page | [<] previous page".center(120))
    print("\n")

  else:
    if length_page < 11:

      for i in range(0, length_page, 1):
        number_session = replace_zero_by_escape(str((count_page*20+i+1)).zfill(3))
        winrate = replace_zero_by_escape(str(page[i]['resolve']*ratio).zfill(3))
        print_session = "| {} -- {} -- {}% |".format(number_session, page[i]['date'], winrate).center(120)
        print(print_session)
    else:

      for i in range(0, 10, 1):
        number_session_left = replace_zero_by_escape(str((count_page*20) + i+1).zfill(3))
        number_session_right = replace_zero_by_escape(str((1*count_page*20) + i+11).zfill(3))

        if i+10 < length_page:
          winrate = replace_zero_by_escape(str(page[i]['resolve']*ratio).zfill(3))
          winrate_10 = replace_zero_by_escape(str(page[i+10]['resolve']*ratio).zfill(3))
          print_session = "| {} -- {} -- {}% || {} -- {} -- {}% |".format(number_session_left,
          page[i]['date'], winrate, number_session_right, page[i+10]['date'], winrate_10).center(120)
          print(print_session)
        else:
          winrate = replace_zero_by_escape(str(page[i]['resolve']*ratio).zfill(3))
          print_session = "| {} -- {} -- {}% |".format(number_session_left, page[i]['date'], winrate).rjust(60)
          print(print_session)

    print("\n")
    print(string_foot.center(120))
    print("\n")
    print("\n")
    print("[{}-{}] more about session | [Q] --> quit | [B] --> back | [>] --> next page | [<] --> previous page".format(count_page*20+1,
    count_page*20+length_page).center(120))
    print("\n")
##
##
def print_session_overview_exercise_basic_focus(number, type_obj, date, time, winrate, total_try, find, not_find, id_puzzles):
  """Session 13 on the str object on 24-02-2020 11:55. Duration 1min for a winrate of 7%.
                          - - Methods find - -
                          lower() | upper ()
                                ....
  """
  os.system('cls' if os.name == 'nt' else 'clear')
  if type_obj == 'tuple':
    ratio = 2
  else:
    ratio = 10
  methods = []
  for method in find:
    if method not in methods:
      methods.append(method)

  print("KNOWLEDGE".center(120,'-'))
  print("\n")
  print("Session {} - Exercise on {} object on {}. Duration {}min for solve {}/{}.".format(number,
  type_obj.upper(), date, time, winrate, ratio).center(120))
  if len(find) != 0:
    average_method_try = total_try / len(find)
  else:
    average_method_try = 0
  print("Average by method: {:.2f} tries | total tries: {}.".format(average_method_try, total_try).center(120))
  print("\n")
  print("- - Methods find in puzzle- -".center(120))
  print("\n")
  print_methods(methods, [], True)
  if not_find:
    print("[L] --> watch a lesson pour the unsolved puzzles".center(120))
    print("\n")
###########################################################################
############################ G R A P H ####################################
###########################################################################
#
def print_menu_graph():
  """- - - Graph of - - - 
      1 - str        23
      2 - list       4
      ... 
      7 - native fc  12 
  """
  list_total_sessions_method = get_data_list_total_sessions_methods()
  list_total_sessions_exercise = get_data_list_total_sessions_exercise_basic()
  def addition(a,b):
    return a+b
  list_total_sessions = list(map(addition, list_total_sessions_method, list_total_sessions_exercise))
  os.system('cls' if os.name == 'nt' else 'clear')
  print("KNOWLEDGE".center(120,'-'))
  print("\n")
  string_menu_graph = "-------- Graph of --------"
  string_menu_graph_foot = "-"*len(string_menu_graph)
  print(string_menu_graph.center(120))
  print("\n")
  for i in range(0, len(OBJECTS)):
    n_sessions = replace_zero_by_escape(str(list_total_sessions[i]).zfill(3))
    string = "{} - {}".format((i+1), OBJECTS[i]).ljust(17)
    string = string+'{}'.format(n_sessions)
    print(string.center(120))

  print("\n")
  print(string_menu_graph_foot.center(120))
  print("\n\n\n")
  print("[Q] --> quit | [B] --> back".center(120))
  print("\n")
##
##
def print_menu_graph_focus(type_obj):
  """------Average of sessions by ------
          1 - Find method - The winrate
          2 - Find method - % methods found
          3 - Find method - All the tries
          4 - Find method - Sessions by day
  """
  os.system('cls' if os.name == 'nt' else 'clear')
  data_find_method = get_data_list_total_sessions_methods()
  index = OBJECTS.index(type_obj)
  data_find_method = data_find_method[index]
  data_exercise = get_data_list_total_sessions_exercise_basic()
  data_exercise = data_exercise[index]
  print("KNOWLEDGE".center(120,'-'))
  print("\n")
  string_menu_graph_focus = "-------- Average of sessions by --------"
  string_menu_graph_focus_foot = "-"*len(string_menu_graph_focus)
  print(string_menu_graph_focus.center(120))
  print("\n")
  string_1 = "1 - Find method - The winrate".ljust(40)
  string_1 = string_1+'{}'.format(data_find_method)
  string_2 = "2 - Find method - % methods found".ljust(40)
  string_2 = string_2+'{}'.format(data_find_method)
  string_3 = "3 - Find method - All the Tries".ljust(40)
  string_3 = string_3+'{}'.format(data_find_method)
  string_4 = "4 - Find method - Sessions by day".ljust(40)
  string_4 = string_4+'{}'.format(data_find_method)
  string_5 = "5 - Exercise - The winrate".ljust(40)
  string_5 = string_5+'{}'.format(data_exercise)
  string_6 = "6 - Exercise - Sessions by day".ljust(40)
  string_6 = string_6+'{}'.format(data_exercise)
  print(string_1.center(120))
  print(string_2.center(120))
  print(string_3.center(120))
  print(string_4.center(120))
  print(string_5.center(120))
  print(string_6.center(120))
  print("\n")
  print(string_menu_graph_focus_foot.center(120))
  print("\n\n\n")
  print("[Q] --> quit | [B] --> back | [M] --> menu".center(120))
  print("\n")
##
##
###########################################################################
############################ LIBRARY ####################################
###########################################################################
#
def print_menu_library():
  """--------- Library ---------
      1 - str
      2- list
      ...
      7 - native fc
  """
  os.system('cls' if os.name == 'nt' else 'clear')
  print("KNOWLEDGE".center(120,'-'))
  print("\n\n")
  string_menu_library = "----- Library -----"
  string_menu_library_foot = "-"*len(string_menu_library)
  print(string_menu_library.center(120))
  print("\n")
  for i in range(0, len(OBJECTS)):
    string = "{} - {}".format((i+1), OBJECTS[i]).ljust(13)
    print(string.center(120))

  print("\n")
  print(string_menu_library_foot.center(120))
  print("\n\n\n")
  print(" [Q] --> quit | [B] --> back".center(120))
  print("\n")
##
##
def print_start_library_method_header(type_obj):
  """----------- All method of the type_obj object ---------------"""

  os.system('cls' if os.name == 'nt' else 'clear')
  print("KNOWLEDGE".center(120,'-'))
  print("\n\n")
  string_library = "------------- All methods of {} -------------".format(type_obj)
  string_library_foot = "-"*len(string_library)
  print(string_library.center(120))
  print("\n")
  return string_library_foot
##
##
def print_start_library_method(list, string_foot):
  """----------- Library of the type_obj ---------------
  |   1 - upper()   ||  4 - capitalize() |
  |   2 - lower()   ||  5 - isnumeric()  |
  """
  max = get_longest_item(list)
  length = len(list)
  col = int(120/max)
  row = int((length / col) + 1)

  if length < 13:
    for i in range(0, length):
      count_method = replace_zero_by_escape(str(i+1).zfill(2))
      string = list[i].ljust(max+1)
      final_string = "{} - {}".format(count_method, string)
      print(final_string.center(120))
      
  elif length >= 13 and length < 25:
    
    for i in range(0, 12):
      count_method_1 = replace_zero_by_escape(str(i+1).zfill(3))
      count_method_2 = replace_zero_by_escape(str(i+13).zfill(3))

      if i + 12 < length:
        string_1 = list[i].ljust(max+1)+'|'
        string_2 = list[i + 12].ljust(max+1)+'|'
        final_string = "| {} - {}| {} - {}".format(count_method_1, string_1, count_method_2, string_2)
        print(final_string.center(120))
      
      else: 
        string_1 = list[i].ljust(max+1)+'|'
        final_string = "| {} - {}".format(count_method_1, string_1)
        print(final_string.rjust(60))
  
  elif length >= 25 and length < 37:

    for i in range(0, 12):
      count_method_1 = replace_zero_by_escape(str(i+1).zfill(3))
      count_method_2 = replace_zero_by_escape(str(i+13).zfill(3))
      count_method_3 = replace_zero_by_escape(str(i+25).zfill(3))

      if i + 24 < length:
        string_1 = list[i].ljust(max+1)+'|'
        string_2 = list[i + 12].ljust(max+1)+'|'
        string_3 = list[i + 24].ljust(max+1)+'|'
        final_string = "| {} - {}| {} - {}| {} - {}".format(count_method_1, string_1,
        count_method_2, string_2, count_method_3, string_3)
        print(final_string.center(120))
      
      else:
        string_1 = list[i].ljust(max+1)+'|'
        string_2 = list[i + 12].ljust(max+1)+'|'
        final_string = "| {} - {}| {} - {}".format(count_method_1, string_1, count_method_2, string_2)
        print(final_string.rjust(72))

  elif length >= 37:

    for i in range(0, 12):
      count_method_1 = replace_zero_by_escape(str(i+1).zfill(3))
      count_method_2 = replace_zero_by_escape(str(i+13).zfill(3))
      count_method_3 = replace_zero_by_escape(str(i+25).zfill(3))
      count_method_4 = replace_zero_by_escape(str(i+37).zfill(3))
      count_method_5 = replace_zero_by_escape(str(i+49).zfill(3))

      if i + 48 < length:
        string_1 = list[i].ljust(max+1)+'|'
        string_2 = list[i + 12].ljust(max+1)+'|'
        string_3 = list[i + 24].ljust(max+1)+'|'
        string_4 = list[i + 36].ljust(max+1)+'|'
        string_5 = list[i + 48].ljust(max-4)+'|'
        final_string = "| {} - {}| {} - {}| {} - {}| {} - {}| {} - {}".format(count_method_1, string_1, count_method_2, string_2,
        count_method_3, string_3, count_method_4, string_4, count_method_5, string_5)
        print(final_string.center(120))
      
      else:
        string_1 = list[i].ljust(max+1)+'|'
        string_2 = list[i + 12].ljust(max+1)+'|'
        string_3 = list[i + 24].ljust(max+1)+'|'
        string_4 = list[i + 36].ljust(max+1)+'|'
        final_string = "| {} - {}| {} - {}| {} - {}| {} - {}".format(count_method_1, string_1, count_method_2, string_2,
        count_method_3, string_3, count_method_4, string_4)
        print(final_string)
  
  print("\n")
  print(string_foot.center(120))
  print("\n\n")
  print("[Q] --> quit | [B] --> back | [M] --> menu | [<] --> previous | [>] --> next".center(120))
  print("\n")
  return
##
##
def print_library_focus(number, type_obj):
  os.system('cls' if os.name == 'nt' else 'clear')
  methods = []
  print(number)
  for method in LIBRARY[type_obj].keys():
    methods.append(method)
  
  print("KNOWLEDGE".center(120,'-'))
  print("\n")
  string = "---------- {} --- {} ----------".format(type_obj.upper(), LIBRARY[type_obj][methods[number-1]]['name'])
  string_foot = "-"*len(string)
  print(string.center(120))
  print("\n")
  definition = LIBRARY[type_obj][methods[number-1]]['def']
  parameter = LIBRARY[type_obj][methods[number-1]]['parameter']
  example1 = LIBRARY[type_obj][methods[number-1]]['example1']
  example2 = LIBRARY[type_obj][methods[number-1]]['example2']

  

  for i in definition:
    print("               "+i)

  print("\n")


  for i in parameter:
    print("               "+i)

  print("\n")

  if len(example1) >= len(example2):

    for i in range(0, len(example1)):
      string_1 = example1[i].ljust(45)

      if i < len(example2):
        string_2 = example2[i].ljust(45)
      else:
        string_2 = ''

      print("               {}  {}".format(string_1, string_2))
  
  print("\n")
  print(string_foot.center(120))
  print("\n")
  print("[Q] --> quit | [B] --> back | [M] --> menu | [<] --> previous | [>] --> next".center(120))
  print("\n")
##
##
def print_review_exercise(method, type_obj, index, length):
  os.system('cls' if os.name == 'nt' else 'clear')
  print("KNOWLEDGE".center(120,'-'))
  print("\n")
  string = "------- {} --- {} ---- review {} of {} ----".format(type_obj.upper(), LIBRARY[type_obj][method]['name'], index+1, length)
  string_foot = "-"*len(string)
  print(string.center(120))
  print("\n")
  definition = LIBRARY[type_obj][method]['def']
  parameter = LIBRARY[type_obj][method]['parameter']
  example1 = LIBRARY[type_obj][method]['example1']
  example2 = LIBRARY[type_obj][method]['example2']

  

  for i in definition:
    print("               "+i)

  print("\n")


  for i in parameter:
    print("               "+i)

  print("\n")

  if len(example1) >= len(example2):

    for i in range(0, len(example1)):
      string_1 = example1[i].ljust(45)

      if i < len(example2):
        string_2 = example2[i].ljust(45)
      else:
        string_2 = ''

      print("               {}  {}".format(string_1, string_2))
  
  print("\n")
  print(string_foot.center(120))
  print("\n")
  print("[Q] --> quit | [M] --> menu  [B] --> back | [<] --> previous | [>] --> next".center(120))
  print("\n")
  
###########################################################################
###################### OTHER FONCTIONALITY ################################
###########################################################################
#
def print_methods(methods, find_methods, end = False):
  """---------14h16: 1min - -STR - - try 0 ---------
      ********** | ********* | ********** |
      *******    | upper()   | ********** |
  """
  max = get_longest_item(methods) + 3
  col = int(119 / max)
  row = int((len(methods) / col)+1)

  for i in range(0, row):
    print_string = ""

    for j in range(0, col):
      index = i*col + j

      if index >= len(methods):
        break
      
      if find_methods:

        if find_methods[index] == 0 and not end:
          show_method = '*'*len(methods[index])
        else:
          show_method = methods[index]
      
      else:
        show_method = methods[index]

      n_ljust = max-1

      if j == col-1:
        print_string += "  {}".format(show_method).ljust(n_ljust)+" "
      else:
        print_string += "  {}".format(show_method).ljust(n_ljust)+" |"

    if row == 1:
      print(print_string.center(120)) if len(print_string) > 0 else None
    
    else:
      print(print_string) if len(print_string) > 0 else None

  print("\n")
##
##


