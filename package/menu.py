import sys
from donnees import *
from donnees_library import *
from package.user import *
from package.tools import *
from package.print import *
from package.data import *
from package.menu import *
from package.graph import *

def start_menu():
  """---------Menu---------
      1 - Find methods
      2- Exercise
      3 - overview of sessions
      4 - see graph
      5 - Library
  """
  choice = get_answer_menu()

  if choice.upper() == 'Q':
    os.system('cls' if os.name == 'nt' else 'clear')
    sys.exit()
  
  elif choice == '1':
    start_menu_method()
  
  elif choice == '2':
    start_menu_exercise()

  elif choice == '3':
    start_menu_overview()

  elif choice == '4':
    start_menu_graph()
  
  elif choice == '5':
    start_menu_library()
  
  choice = get_answer_menu_or_leave()

  if choice.upper() == 'M':
    start_menu()
  
  elif choice.upper() == 'Q':
    os.system('cls' if os.name == 'nt' else 'clear')
    sys.exit()

###########################################################################
####################### SESSION FIND METHODE ##############################
###########################################################################
def start_menu_method():
  """---------Find methods of---------
      1 - str
      2- list
      ...
      7 - native fc
  """
  choice = get_answer_menu_method()

  if type(choice) == int:
    start_session_method(OBJECTS[choice-1]) # replace game_session_method
    
  else:
    if choice.upper() == 'Q':
      os.system('cls' if os.name == 'nt' else 'clear')
      sys.exit()

    elif choice.upper() == 'B':
      start_menu()
  
    elif choice.upper() == 'R':
      type_object = randint(0, len(OBJECTS))
      results_session = start_session_method(OBJECTS[type_object]) 

      if results_session:
        add_session_method(results_session)
#
#
#
def start_session_method(type_obj):
  """---------14h16: 1min - -STR - - try 0 ---------
      ********** | ********* | ********** |
      *******    | upper()   | ********** |
  """

  #*************INITIALIZE VARIABLE*************
  start_date = datetime.datetime.now().strftime("%d-%m-%Y %H:%M")
  study = type_obj

  methods = simplify_methods(type_obj)
  index_find = [0]*len(methods)
  start_hour = datetime.datetime.now().strftime("%H")
  start_minute = datetime.datetime.now().strftime("%M")
  total_try = 0
  name = " - - - {} - - - ".format(type_obj.upper())
  user_method = ''
  last_user_method = ''
  fail_in_a_row = 0
  #**********************************************

  while 0 in index_find:
    # While the user don't prompt 'Q', find all methods or fail 10 in a row, he continue :)
    # we get some value like duration of session, count of try, number of methods find.

    current_date = datetime.datetime.now().strftime("%M")
    duration = ((60 - int(start_minute) +int(current_date)) % 60) + 1
    os.system('cls' if os.name == 'nt' else 'clear')
    
    #*******************DISPLAY******************
    print("KNOWLEDGE".center(120,'-'))
    print("\n")
    print_info = start_hour+"h"+start_minute+": "+str(duration)+"min  "+ name + "   try: "+str(total_try)
    print(print_info.center(120))
    print("\n")
    print_methods(methods, index_find)
    #********************************************
    
    # Condition for display or Quit across the total of try.
    if fail_in_a_row > 9:
      print("You fail 10 in a row, better close this session.")
      break

    if last_user_method != '' or total_try !=0:

      if fail_in_a_row > 4:
        print("last :  {}. You fail {} in a row. Maybe time to stop.".format(last_user_method, fail_in_a_row)
        .rjust(102+len(last_user_method)))
      else:
        print("last :  {}".format(last_user_method).rjust(60+len(last_user_method)))

    else:
      print("")

    print("\n")
    print("example with parameter: method(...) | no parameter: method()".center(120))
    print("\n")
    print("[Q] --> quit | [E] --> end".center(120))
    print("\n")
    user_method = input("Find an method: ".rjust(67))
    
    # See if the user's method is good, or increase the total fail and fail in a row count.
    if user_method in TYPE_OF[type_obj]:
      index_method = TYPE_OF[type_obj].index(user_method)
      index_find[index_method] = 1
      fail_in_a_row = 0

    elif user_method.upper() == 'Q':
      os.system('cls' if os.name == 'nt' else 'clear')
      sys.exit()

    elif user_method.upper() == 'E':
      break

    elif user_method != '':
      fail_in_a_row += 1

    
    last_user_method = user_method

    if user_method != '':
      total_try += 1
  
  # We are outside the big WHILE so, the session is finish
  # [0] --> duration | [1] --> winrate | [2] --> find | [3] --> not_find
  choice = ''
  while choice.upper() != 'Q' and choice.upper() != 'M' and choice.upper() != 'L':
    results = print_method_end(index_find, duration, type_obj, total_try)

    if results[4]:
      print("[L] --> See lessons for les not find methods".center(120))
      print("\n")
    print("[Q] --> quit | [M] --> menu".center(120))
    print("\n")
    choice = input('Your choice: '.rjust(65))

  result_of_session = {
    'type_obj': type_obj,
    'date': start_date,
    'duration': results[0],
    'winrate': results[1],
    'tries': results[2],
    'find': results[3],
    'not_find': results[4]
  }

  if results[3]: 
    add_session_method(result_of_session)

  if choice.upper() == 'Q':
    os.system('cls' if os.name == 'nt' else 'clear')
    sys.exit()
  
  elif choice.upper() == 'M':
    start_menu()
  
  elif choice.upper() == 'L' and result_of_session['not_find']:

    lessons = result_of_session['not_find']
    for i in range(0, len(lessons)):
      if lessons[i].find('(') != -1:
        x = lessons[i].find('(')
        lessons[i] = lessons[i][:x]

    list_string = []
    for lesson in lessons:
      if lesson not in list_string:
        list_string.append(lesson)

    review_exercise(list_string, type_obj, before="find")

#----------------------------------------------------------------------------
#--########################################################################--
#--####################### SESSION EXERCISE ###############################--
#--########################################################################--
#----------------------------------------------------------------------------
def start_menu_exercise():
  """-----Exercise-----
        1 - str
        2 - list
        ...
        8 - expert
  """
  choice = get_answer_menu_exercise()

  if type(choice) == int:
    start_session_exercise_basic(OBJECTS[choice-1])
    
  else:
    if choice.upper() == 'Q':
      os.system('cls' if os.name == 'nt' else 'clear')
      sys.exit()

    elif choice.upper() == 'B':
      start_menu()
    
##
##
#
#------------------------------------------------------
#------------- SESSION EXERCISE BASIC -----------------
#------------------------------------------------------
#
def start_session_exercise_basic(type_obj):
  """retrieve data session exercise basic and display:
    --- 14h21 --> 1min tries 5 - Exercise basic of {} solved - [x] of [10] ---
    Question about the current type_obj
  """
  file_path = PATH_EXERCISE+type_obj

  sessions = get_data_sessions_exercise_basic(file_path)
  if type_obj == 'tuple':
    puzzles = get_10_basic_puzzles(type_obj, 2)
    length_puzzle = 2
  else:
    puzzles = get_10_basic_puzzles(type_obj, 10)
    length_puzzle = 10

  start_date = datetime.datetime.now().strftime("%d-%m-%Y %H:%M")
  start_hour = datetime.datetime.now().strftime("%H")
  start_minute = datetime.datetime.now().strftime("%M")
  print_hour = start_hour+'h'+start_minute
  total_try, resolve, index_puzzle = 0, 0, 0
  id_puzzles, solve_puzzles, not_solve_puzzles = [], [], []
  for puzzle in puzzles:
    id_puzzles.append(puzzle['id'])

  while index_puzzle < length_puzzle:
    current_date = datetime.datetime.now().strftime("%M")
    duration = ((60 - int(start_minute) +int(current_date)) % 60) + 1

    choice, tries_count = get_answer_exercise_basic(puzzles, index_puzzle, type_obj, duration, print_hour)
    total_try += tries_count

    if choice.upper() != 'Q' and choice.upper() != 'E' and choice.upper() != 'N' and choice != 'lose':
      if choice not in solve_puzzles:
        solve_puzzles.append(choice)
      resolve += 1
      index_puzzle += 1
    
    elif choice.upper() == 'Q':
      os.system('cls' if os.name == 'nt' else 'clear')
      sys.exit()
    
    elif choice.upper() == 'E':
      for i in range(index_puzzle, length_puzzle):
        if puzzles[i]['solution'] not in not_solve_puzzles:
          not_solve_puzzles.append(puzzles[i]['solution'][0])
      break
    
    elif choice.upper() == 'N':
      if puzzles[index_puzzle]['solution'] not in not_solve_puzzles:
        not_solve_puzzles.append(puzzles[index_puzzle]['solution'][0])
      index_puzzle += 1

    elif choice == 'lose':
      if puzzles[index_puzzle]['solution'] not in not_solve_puzzles:
        not_solve_puzzles.append(puzzles[index_puzzle]['solution'][0])
      index_puzzle += 1
    
  not_solve_puzzles_clean = []
  for i in range(0, len(not_solve_puzzles)):
    if not_solve_puzzles[i].find(',') != -1:
      x = not_solve_puzzles[i].find(',')
      not_solve_puzzles[i] = not_solve_puzzles[i][:x]

    if not_solve_puzzles[i] not in not_solve_puzzles_clean:
      not_solve_puzzles_clean.append(not_solve_puzzles[i])
  
  solve_puzzles_clean = []
  for i in range(0, len(solve_puzzles)):
    if solve_puzzles[i].find(',') != -1:
      x = solve_puzzles[i].find(',')
      solve_puzzles[i] = solve_puzzles[i][:x]
    
    solve_puzzles_clean.append(solve_puzzles[i])

  results = {
    'type_obj': type_obj,
    'date': start_date,
    'duration': duration,
    'resolve': resolve,
    'tries': total_try,
    'solve': solve_puzzles_clean,
    'not_solve': not_solve_puzzles_clean,
    'id_puzzles': id_puzzles,
  }
  
  if resolve:
    add_session_exercise_basic(results)

  choice = get_answer_exercise_basic_end(duration, resolve, total_try, type_obj)

  if choice.upper() == 'Q':
    os.system('cls' if os.name == 'nt' else 'clear')
    sys.exit()
  
  elif choice.upper() == 'M':
    start_menu()

  elif choice.upper() == 'L':

    lessons = results['not_solve']
    list_string = []
    for lesson in lessons:
      if lesson not in list_string:
        list_string.append(lesson)

    review_exercise(list_string, type_obj, before='exercise')
##
##
def start_session_exercise_expert():
  """retrieve data session exercise expert and display:
    --- 14h21 --> 1min tries 5 - Exercise basic of {} solved - [x] of [10] ---
    Question expert
  """
  file_path = PATH_EXPERT

  sessions = get_data_sessions_exercise_expert(file_path)

  #recréer le puzzle
  puzzles = get_10_expert_puzzles('str', 10)

  start_date = datetime.datetime.now().strftime("%d-%m-%Y %H:%M")
  start_hour = datetime.datetime.now().strftime("%H")
  start_minute = datetime.datetime.now().strftime("%M")
  print_hour = start_hour+'h'+start_minute
  total_try, resolve, index_puzzle = 0, 0, 0
  id_puzzles, solve_puzzles, not_solve_puzzles = [], [], []
  for puzzle in puzzles:
    id_puzzles.append(puzzle['id'])

  while index_puzzle < 10:
    current_date = datetime.datetime.now().strftime("%M")
    duration = ((60 - int(start_minute) +int(current_date)) % 60) + 1

    choice, tries_count = get_answer_exercise_expert(puzzles, index_puzzle, duration, print_hour)
    total_try += tries_count

    if choice.upper() != 'Q' and choice.upper() != 'E' and choice.upper() != 'N' and choice != 'lose':
      if choice not in solve_puzzles:
        solve_puzzles.append(choice)
      resolve += 1
      index_puzzle += 1
    
    elif choice.upper() == 'Q':
      os.system('cls' if os.name == 'nt' else 'clear')
      sys.exit()
    
    elif choice.upper() == 'E':
      for i in range(index_puzzle, 10):
        if puzzles[i]['solution'] not in not_solve_puzzles:
          not_solve_puzzles.append(puzzles[i]['solution'][0])
      break
    
    elif choice.upper() == 'N':
      if puzzles[index_puzzle]['solution'] not in not_solve_puzzles:
        not_solve_puzzles.append(puzzles[index_puzzle]['solution'][0])
      index_puzzle += 1

    elif choice == 'lose':
      if puzzles[index_puzzle]['solution'] not in not_solve_puzzles:
        not_solve_puzzles.append(puzzles[index_puzzle]['solution'][0])
      index_puzzle += 1
    
  not_solve_puzzles_clean = []
  for i in range(0, len(not_solve_puzzles)):
    if not_solve_puzzles[i].find(',') != -1:
      x = not_solve_puzzles[i].find(',')
      not_solve_puzzles[i] = not_solve_puzzles[i][:x]

    if not_solve_puzzles[i] not in not_solve_puzzles_clean:
      not_solve_puzzles_clean.append(not_solve_puzzles[i])
  
  solve_puzzles_clean = []
  for i in range(0, len(solve_puzzles)):
    if solve_puzzles[i].find(',') != -1:
      x = solve_puzzles[i].find(',')
      solve_puzzles[i] = solve_puzzles[i][:x]
    
    solve_puzzles_clean.append(solve_puzzles[i])

  results = {
    'date': start_date,
    'duration': duration,
    'resolve': resolve,
    'tries': total_try,
    'solve': solve_puzzles_clean,
    'not_solve': not_solve_puzzles_clean,
    'id_puzzles': id_puzzles,
  }
  
  if resolve:
    add_session_exercise_expert(results)

  choice = get_answer_exercise_expert_end(duration, resolve, total_try)

  if choice.upper() == 'Q':
    os.system('cls' if os.name == 'nt' else 'clear')
    sys.exit()
  
  elif choice.upper() == 'M':
    start_menu()


##
##
#
#------------------------------------------------------
#------------ SESSION EXERCISE EXPERT -----------------
#------------------------------------------------------
#
#  * * * * * * * * TO PRODUCT  * * * * * * * * * 
##
##
#
#------------------------------------------------------
#------------- SESSION EXERCISE HARD ------------------
#------------------------------------------------------
#
#  * * * * * * * * TO PRODUCT  * * * * * * * * * 
##
##
#---------------------------------------------------------------------------
#--#######################################################################--
#--###################### SESSION OVERVIEW ###############################--
#--#######################################################################--
#---------------------------------------------------------------------------
def start_menu_overview():
  """------ Overview of ------
      1 - Find methods      12
      2 - Exercise basic    3
      3 - Exercise expert   42
      4 - Exercise hard     24
      5 - All exercises     
  """
  choice = get_answer_menu_overview()

  if type(choice) == int:
      if choice == 1:
        start_menu_overview_method()
      elif choice == 2:
        start_menu_overview_exercise_basic()
        pass

  else:
    if choice.upper() == 'Q':
      os.system('cls' if os.name == 'nt' else 'clear')
      sys.exit()

    elif choice.upper() == 'B':
      start_menu()

  return
#
#------------------------------------------------------
#----------- SESSIONS OVERVIEW METHOD -----------------
#------------------------------------------------------
#
def start_menu_overview_method():
  """- - - overview find methods - - - 
            1 - str 
            2 - list 
            ... 
            7 - native fc"""

  choice = get_answer_menu_overview_method()

  if type(choice) == int:
      start_session_overview_method(OBJECTS[choice-1])

  else:
    if choice.upper() == 'Q':
      os.system('cls' if os.name == 'nt' else 'clear')
      sys.exit()

    elif choice.upper() == 'B':
      start_menu_overview()

  return
##
##
def start_session_overview_method(type_obj):
  """----------- All sessions of type_obj page [x] of [y] ---------------
  |   1 -- 24-02-2022 11:55 --   7% ||  11 -- 24-02-2022 14:26 --   3% |
  |   2 -- 24-02-2022 13:39 --  13% ||  12 -- 24-02-2022 14:34 --   3% |
  """
  file_path = PATH_FIND_METHOD+type_obj
  all_sessions = get_data_sessions_methods(file_path)

  choice = get_answer_start_session_overview_method(all_sessions, type_obj)

  if type(choice) == int:
    start_session_overview_method_focus(file_path, choice, type_obj)

  else:
    if choice.upper() == 'Q':
      os.system('cls' if os.name == 'nt' else 'clear')
      sys.exit()
      return
      
    elif choice.upper() == 'B':
      start_menu_overview_method()

  return
##
##
def start_session_overview_method_focus(path, number, type_obj):
  """----------- All sessions of type_obj page [x] of [y] ---------------
  Session 1 on the str object on 24-02-2020 11:55. Duration 1min for a winrate of 7%.
                          - - Methods find - -
                          lower() | upper ()
                                ....
  """
  session = get_session_overview_method_focus(path, number)
  type_obj, date, time, winrate = session['type_obj'], session['date'], session['duration'], session['winrate']
  total_try, find, not_find = session['tries'], session['find'], session['not_find']

  choice = get_answer_session_overview_method_focus(number, type_obj, date, time, winrate, total_try, find, not_find)

  if choice.upper() == 'Q':
    os.system('cls' if os.name == 'nt' else 'clear')
    sys.exit()

  elif choice.upper() == 'B':
    start_session_overview_method(type_obj)

  elif choice.upper() == 'M':
    start_menu()
##
##
#------------------------------------------------------
#------- SESSIONS OVERVIEW EXERCISE BASIC -------------
#------------------------------------------------------
#
def start_menu_overview_exercise_basic():
  """- - - overview exercise basic - - - 
            1 - str 
            2 - list 
            ... 
            7 - native fc"""
  choice = get_answer_menu_overview_exercise_basic()

  if type(choice) == int:
      start_session_overview_exercise_basic(OBJECTS[choice-1])

  else:
    if choice.upper() == 'Q':
      os.system('cls' if os.name == 'nt' else 'clear')
      sys.exit()

    elif choice.upper() == 'B':
      start_menu_overview()
##
##
def start_session_overview_exercise_basic(type_obj):
  """----------- All sessions exercise basic of type_obj page [x] of [y] ---------------
  |   1 -- 24-02-2022 11:55 --   70% ||  11 -- 24-02-2022 14:26 --   30% |
  |   2 -- 24-02-2022 13:39 --   10% ||  12 -- 24-02-2022 14:34 --   50% |
  """
  file_path = PATH_EXERCISE+type_obj
  all_sessions = get_data_sessions_exercise_basic(file_path)

  choice = get_answer_start_session_overview_exercise_basic(all_sessions, type_obj)

  if type(choice) == int:
    start_session_overview_exercise_basic_focus(file_path, choice, type_obj)

  else:
    if choice.upper() == 'Q':
      os.system('cls' if os.name == 'nt' else 'clear')
      sys.exit()
      return
      
    elif choice.upper() == 'B':
      start_menu_overview_exercise_basic()

  return
##
##
def start_session_overview_exercise_basic_focus(path, number, type_obj):

  session = get_session_overview_method_focus(path, number)
  type_obj, date, time, winrate = session['type_obj'], session['date'], session['duration'], session['resolve']
  total_try, find, not_find, id_puzzles = session['tries'], session['solve'], session['not_solve'], session['id_puzzles']

  choice = get_answer_session_overview_exercise_basic_focus(number, type_obj, date, time, winrate, total_try, find, not_find, id_puzzles)

  if choice.upper() == 'Q':
    os.system('cls' if os.name == 'nt' else 'clear')
    sys.exit()

  elif choice.upper() == 'B':
    start_session_overview_exercise_basic(type_obj)

  elif choice.upper() == 'M':
    start_menu()
  
  elif choice.upper() == 'L':
    lessons = not_find
    if lessons:
      list_string = []

      for lesson in lessons:
        if lesson not in list_string:
          list_string.append(lesson)

      review_exercise(list_string, type_obj, before='exercise')
    else:
      start_session_overview_exercise_basic_focus(path, number, type_obj)
##
##
###########################################################################
############################ G R A P H ####################################
###########################################################################

def start_menu_graph():
  """- - - Graph of - - - 
      1 - str        23
      2 - list       4
      ... 
      7 - native fc  12 
  """
  choice = get_answer_menu_graph()

  if type(choice) == int:
      start_menu_graph_focus(OBJECTS[choice-1])

  else:
    if choice.upper() == 'Q':
      os.system('cls' if os.name == 'nt' else 'clear')
      sys.exit()

    elif choice.upper() == 'B':
      start_menu()

  return
##
##
def start_menu_graph_focus(type_obj):
  """------Average of sessions by ------
          1 - The winrate
          2 - % time methods is found
          3 - All the tries
          4 - Sessions by day
  """
  choice = get_answer_start_menu_graph_focus(type_obj)

  if type(choice) == int:

    if choice == 1:
      winrate_graph = WinrateGraph(type_obj)
      winrate_graph.show('plot')
      start_menu_graph_focus(type_obj)
    
    elif choice == 2:
      methods_graph = MethodsGraph(type_obj)
      methods_graph.show('bar')
      start_menu_graph_focus(type_obj)

    elif choice == 3:
      tries_method_graph = TriesByMethodGraph(type_obj)
      tries_method_graph.show('plot')
      start_menu_graph_focus(type_obj)

    elif choice == 4:
      session_day_graph = SessionByDayGraph(type_obj)
      session_day_graph.show('plot')
      start_menu_graph_focus(type_obj)
    
    elif choice == 5:
      exercise_winrate_graph = ExerciseWinrateGraph(type_obj)
      exercise_winrate_graph.show('plot')
      start_menu_graph_focus(type_obj)

    elif choice == 6:
      exercise_day_graph = ExerciseDayGraph(type_obj)
      exercise_day_graph.show('plot')
      start_menu_graph_focus(type_obj)

  else:
    if choice.upper() == 'Q':
      os.system('cls' if os.name == 'nt' else 'clear')
      sys.exit()
      return
      
    elif choice.upper() == 'B':
      start_menu_graph()

    elif choice.upper() == 'M':
      start_menu()

  return
##
##
###########################################################################
############################# LIBRARY #####################################
###########################################################################
def start_menu_library():
  """--------- Library ---------
      1 - str
      2- list
      ...
      7 - native fc
  """
  choice = get_answer_menu_library()

  if type(choice) == int:
    start_library(OBJECTS[choice-1]) # replace game_session_method
    
  else:
    if choice.upper() == 'Q':
      os.system('cls' if os.name == 'nt' else 'clear')
      sys.exit()

    elif choice.upper() == 'B':
      start_menu()
##
##
def start_library(type_obj):
  """----------- Library of the type_obj ---------------
  |   1 - upper()   ||  4 - capitalize() |
  |   2 - lower()   ||  5 - isnumeric()  |
  """
  library = LIBRARY[type_obj]
  choice, type_obj = get_answer_start_library(library, type_obj)

  if type(choice) == int:
    start_library_focus(choice, type_obj)

  else:
    if choice.upper() == 'Q':
      os.system('cls' if os.name == 'nt' else 'clear')
      sys.exit()
      return
      
    elif choice.upper() == 'B':
      start_menu_library()

    elif choice.upper() == 'M':
      start_menu()

  return
##
##
def start_library_focus(choice, type_obj):

  choice = get_answer_library_focus(choice, type_obj)

  if choice.upper() == 'Q':
    os.system('cls' if os.name == 'nt' else 'clear')
    sys.exit()

  elif choice.upper() == 'B':
    start_library(type_obj)

  elif choice.upper() == 'M':
    start_menu()
##
##
def review_exercise(list, type_obj, before):

  print(list)
  choice = get_answer_review_exercise(list, type_obj)

  if choice.upper() == 'Q':
    os.system('cls' if os.name == 'nt' else 'clear')
    sys.exit()

  elif choice.upper() == 'M':
    start_menu()

  elif choice.upper() == 'B':
    if before == 'find':
      start_session_overview_method(type_obj)
    elif before == 'exercise':
      start_session_overview_exercise_basic(type_obj)

