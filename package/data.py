import os
from pickle import Pickler, Unpickler
import datetime
from package.tools import *
from package.print import *
from donnees import *
#
#
###########################################################################
########################## GET ONE SESSION ################################
###########################################################################
#
def get_session_overview_method_focus(path, session):
  """get ONE session {'type_obj': type_obj, 'date':date...} FIND METHOD """

  with open(path, 'rb') as f:
    my_unpickler = Unpickler(f)
    sessions = my_unpickler.load()

  page = int(session / 20)

  data_session = sessions[page][session - (page * 20 + 1)]
  return data_session
##
##
def get_session_overview_exercise_basic_focus(path, session):
  """get ONE session {'type_obj': type_obj, 'date':date...} EXERCISE BASIC  """
  
  with open(path, 'rb') as f:
    my_unpickler = Unpickler(f)
    sessions = my_unpickler.load()

  page = int(session / 20)

  data_session = sessions[page][session - (page * 20 + 1)]
  return data_session
###
###
###########################################################################
######################### GET ALL SESSIONS ################################
###########################################################################
#
def get_data_sessions_methods(file_path):
  """get ALL sessions FIND METHOD
  [{type_obj: 'date'....},
   {type_obj: 'date'....},
    ...]"""

  if os.path.isfile(file_path):

    with open(file_path, 'rb') as f:
      my_unpickler = Unpickler(f)
      data_methods = my_unpickler.load()

  else:
    data_methods = [[]]
  
  return data_methods
##
##
def get_data_sessions_exercise_basic(path):
  """get ALL sessions EXERCICE BASIC
  [{type_obj: 'date'....},
   {type_obj: 'date'....},
    ...]"""

  if os.path.isfile(path):

    with open(path, 'rb') as f:
      my_unpickler = Unpickler(f)
      sessions = my_unpickler.load()
  
  else:
    sessions = [[]]

  return sessions
###
###
def get_data_sessions_exercise_expert(path):

  if os.path.isfile(path):

    with open(path, 'rb') as f:
      my_unpickler = Unpickler(f)
      sessions = my_unpickler.load()
  
  else:
    sessions = [[]]

  return sessions
##
##
###########################################################################
################### LIST OF SESSIONS BY TYPE_OBJ ###########################
###########################################################################
#
def get_data_list_total_sessions_methods():
  """Return [42 sessions str, 23 sessions list, ...,]"""
  list_total_sessions = []

  for type_obj in OBJECTS:
    path = PATH_FIND_METHOD+type_obj

    if os.path.isfile(path):

      with open(path, 'rb') as f:
        my_unpickler = Unpickler(f)
        sessions = my_unpickler.load()
        page = len(sessions)
        total_session = (page - 1) * 20 + len(sessions[-1])
        list_total_sessions.append(total_session)

    else:
      list_total_sessions.append(0)
  return list_total_sessions
##
##
def get_data_list_total_sessions_exercise_basic():
  """Return [11 sessions str, 38 sessions list, ...,]"""

  list_total_sessions = []

  for type_obj in OBJECTS:
    path = PATH_EXERCISE+type_obj

    if os.path.isfile(path):

      with open(path, 'rb') as f:
        my_unpickler = Unpickler(f)
        sessions = my_unpickler.load()
        page = len(sessions)
        total_session = (page - 1) * 20 + len(sessions[-1])
        list_total_sessions.append(total_session)

    else:
      list_total_sessions.append(0)
  return list_total_sessions
###
###
def get_data_list_total_sessions_exercise_expert():

  list_total_sessions = []
  path = PATH_EXPERT

  if os.path.isfile(path):
    with open(path, 'rb') as f:
      my_unpickler = Unpickler(f)
      sessions = my_unpickler.load()
      page = len(sessions)
      total_session = (page - 1) * 20 + len(sessions[-1])
      list_total_sessions.append(total_session)
  
  else:
    list_total_sessions.append(0)
  return list_total_sessions
###########################################################################
########################### ADD SESSION ###################################
###########################################################################
#
def add_session_method(results):
  """add ONE session {'type_obj': type_obj, 'date':date...} FIND METHOD"""

  path = PATH_FIND_METHOD+results['type_obj']
  new_session = results

  page_sessions = get_data_sessions_methods(path)
  last_page = page_sessions[-1]
  if len(last_page) == 20:
    page_sessions.append([])
    last_page = page_sessions[-1]
  
  last_page.append(new_session)
  page_sessions[-1] = last_page


  with open(path, 'wb') as f:
    my_pickler = Pickler(f)
    my_pickler.dump(page_sessions)
##
##
def add_session_exercise_basic(results):
  """add ONE session {'type_obj': type_obj, 'date':date...} EXERCISE BASIC"""

  path = PATH_EXERCISE+results['type_obj']
  
  session = results
  print("add sessoin")
  page_sessions = get_data_sessions_exercise_basic(path)
  last_page = page_sessions[-1]

  if len(last_page) == 20:
    page_sessions.append([])
    last_page = page_sessions[-1]
  
  last_page.append(session)
  page_sessions[-1] = last_page


  with open(path, 'wb') as f:
    my_pickler = Pickler(f)
    my_pickler.dump(page_sessions)
###
###
def add_session_exercise_expert(results):
  path = PATH_EXPERT
  
  session = results
  page_sessions = get_data_sessions_exercise_expert(path)
  last_page = page_sessions[-1]

  if len(last_page) == 20:
    page_sessions.append([])
    last_page = page_sessions[-1]
  
  last_page.append(session)
  page_sessions[-1] = last_page


  with open(path, 'wb') as f:
    my_pickler = Pickler(f)
    my_pickler.dump(page_sessions)

###########################################################################
######################### GET DATA FOR GRAPH ##############################
###########################################################################
#
def get_list_winrate(type_obj):
  """Get data for WinrateGraph:
    [13%, 25%, 45%, 22%, 58%,...,] winrate
    [1, 2, 3, 4, 5, 6, ...,] sessions 
  """
  file_path = PATH_FIND_METHOD+type_obj
  sessions = get_data_sessions_methods(file_path)

  list_n_winrate, list_n_session, count = [], [], 0

  for page in sessions:
    for session in page:
      count += 1
      list_n_winrate.append(session['winrate'])
      list_n_session.append(count)

  return list_n_session, list_n_winrate
##
##
def get_list_method(type_obj):
  """Get data for WinrateGraph:
    [method1, method2, method3...] methode sorted
    [10, 12, 3, 25, 14, ...] number find the respective method 
  """
  file_path = PATH_FIND_METHOD+type_obj
  sessions = get_data_sessions_methods(file_path)

  list_find, count_session = {}, 0
  
  list_method = {method: 0 for method in TYPE_OF[type_obj]}
  print(list_method)

  for page in sessions:
    for session in page:
      for method in session['find']:
        list_method[method] += 1
      count_session += 1
 
  list_name_methods = []
  for key, value in list_method.items():
    list_name_methods.append(key)

  list_name_methods_sorted = sorted(list_name_methods)

  list_value_methods_sorted = []
  for name in list_name_methods_sorted:
    list_value_methods_sorted.append(round((list_method[name] / count_session)*100, 2))
  
  return simplify_list_methods(list_name_methods_sorted), list_value_methods_sorted
##
##
def get_list_tries_method(type_obj):
  """Get data for WinrateGraph:
    [1.3 ,2.7, 1.4, 3.6, 2.9,...] tries by method
    [1, 2, 3, 4, 5, 6, ...] session n° 
  """
  file_path = PATH_FIND_METHOD+type_obj
  sessions = get_data_sessions_methods(file_path)

  list_tries_method, list_n_session, count = [], [], 1

  for page in sessions:

    for session in page:
      tries = session['tries']
      n_find_method = len(session['find'])
      if n_find_method == 0:
        list_tries_method.append(0)
      else:
        list_tries_method.append(round(tries / n_find_method, 2))
      list_n_session.append(count)
      count += 1
  return list_n_session, list_tries_method
##
##
def get_list_session_day(type_obj):
  """Get data for WinrateGraph:
    [23/02, 24/02, 27/02, ...] day where session is done
    [2, 3, 1, 2, 3, 1, ...] count of session respective to the day 
  """
  file_path = PATH_FIND_METHOD+type_obj
  sessions = get_data_sessions_methods(file_path)

  dict_date, list_date, list_count_date = {}, [], []

  for page in sessions:
    for session in page:
      date = session['date'][:10]
      if date in dict_date.keys():
        dict_date[date] += 1
      else:
        dict_date[date] = 1

  for date, count in dict_date.items():
    list_date.append(date)
    list_count_date.append(count)

  return list_date, list_count_date
##
##
def get_list_puzzle_solve(type_obj):
  """Get data for ExerciseWinrateGraph:
    [10, 7, 5, 6, 8,...,] winrate
    [1, 2, 3, 4, 5, 6, ...,] sessions 
  """
  file_path = PATH_EXERCISE+type_obj
  sessions = get_data_sessions_exercise_basic(file_path)

  list_n_solve, list_n_session, count = [], [], 0

  for page in sessions:
    for session in page:
      count += 1
      list_n_solve.append(session['resolve'])
      list_n_session.append(count)
  
  return list_n_session, list_n_solve
##
##
def get_list_exercise_session_day(type_obj):
  """Get data for ExerciseDayGraph:
    [23/02, 24/02, 27/02, ...] day where session is done
    [2, 3, 1, 2, 3, 1, ...] count of session respective to the day 
  """
  file_path = PATH_EXERCISE+type_obj
  sessions = get_data_sessions_exercise_basic(file_path)

  dict_date, list_date, list_count_date = {}, [], []

  for page in sessions:
    for session in page:
      date = session['date'][:10]
      if date in dict_date.keys():
        dict_date[date] += 1
      else:
        dict_date[date] = 1

  for date, count in dict_date.items():
    list_date.append(date)
    list_count_date.append(count)

  return list_date, list_count_date

