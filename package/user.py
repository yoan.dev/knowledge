from package.menu import *
from package.print import *
#
###########################################################################
############################# M E N U #####################################
###########################################################################
#
def get_answer_menu():
  """---------Menu---------
      1 - Find methods
      2- Exercise
      3 - overview of sessions
      4 - see graph
      5 - Library
  """
  choice = ''
  while choice != '1' and choice != '2' and choice != '3' and choice != '4' and choice != '5' and str(choice).upper() != 'Q':
    print_menu()
    choice = input("Your choice: ".rjust(63))
  
  return choice
##
##
def get_answer_menu_or_leave():

  choice = ''
  while choice.upper() != 'Q' and choice.upper() != 'M':
    print("[Q] --> quit | [M] --> menu".center(120))
    print("\n")
    choice = input("Your choice?: ".rjust(65))

  return choice
##
##
###########################################################################
########################### FIND METHOD ###################################
###########################################################################
#
def get_answer_menu_method():
  """---------Find methods of---------
      1 - str
      2- list
      ...
      7 - native fc
  """
  choice = -1
  while choice < 1 or choice > 7:
    print_menu_method()
    choice = input("Your choice?: ".rjust(65))
    if choice.upper() == 'Q' or choice.upper() == 'B' or choice.upper() == 'R':
      return choice
    else:
      try:
        choice = int(choice)
      except ValueError:
        choice = -1

  return choice
##
##
#----------------------------------------------------------------------------
#--########################################################################--
#--############################# EXERCISE #################################--
#--########################################################################--
#----------------------------------------------------------------------------
#
def get_answer_menu_exercise():
  """-----Exercise-----
        1 - str
        2 - list
        ...
        8 - expert
  """
  choice = -1
  while choice < 1 or choice > 7:
    print_menu_exercise()

    choice = input("Your choice?: ".rjust(65))

    if choice.upper() == 'Q' or choice.upper() == 'B':
      return choice
    
    else:
      try:
        choice = int(choice)
      except ValueError:
        choice = -1

  return choice
##
##
#------------------------------------------------
#------------- EXERCISE BASIC -------------------
#------------------------------------------------
#
def get_answer_menu_exercise_basic():
  """ - - - exercise basic - - - 
            1 - str
            2 - list
            ....
            7 - native fc
  """
  choice = -1
  while choice < 1 or choice > 7:
    print_menu_exercise_basic()

    choice = input("Your choice?: ".rjust(65))

    if choice.upper() == 'Q' or choice.upper() == 'B' or choice.upper() == 'R':
      return choice
    
    else:
      try:
        choice = int(choice)
      except ValueError:
        choice = -1

  return choice
##
##
def get_answer_exercise_basic(puzzles, resolve, type_obj, duration, print_hour):
  """--- 14h21 --> 1min tries 5 - Exercise basic of {} solved - [x] of [10] ---
    Question about the current type_obj
  """
  choice = ''
  tries_puzzle = 5
  count_tries = 0

  while choice not in puzzles[resolve]['solution']:
    
    print_exercise_basic(puzzles[resolve], resolve, type_obj, tries_puzzle, duration, print_hour)
    choice = input("Your choice: ".rjust(65))

    if choice != '':
      
      if choice.upper() == 'Q' or choice.upper() == 'E' or choice.upper() == 'N':
        return choice, count_tries

      elif choice.upper() != 'Q' and choice.upper() != 'E' and choice.upper() != 'N':
        tries_puzzle -= 1
        count_tries += 1

      if tries_puzzle == 0:
        choice = 'lose'
        return choice, count_tries
    
  return choice, count_tries
##
##
def get_answer_exercise_basic_end(duration, resolve, total_try, type_obj):
  """The session on STR lasted 1min. You solved 2/10 puzzles in 3 tries. ---
    [L] --> See the specific lesson for the unsolved puzzle
  """
  choice = ''

  while choice.upper() != 'Q' and choice.upper() != 'M':
    print_exercise_basic_end(duration, resolve, total_try, type_obj)
    choice = input("Your choice: ".rjust(65))
    if (choice.upper() == 'L' and resolve != 10 and type_obj != 'tuple') or (choice.upper() == 'L' and resolve != 2 and type_obj == 'tuple'):
      return choice
    # Maybe an option for see directly the current session and look puzzle OR
    #See an package lesson about the methods not find but unsorted.

  return choice
##
##
#------------------------------------------------
#------------- EXERCISE EXPERT -------------------
#------------------------------------------------
def get_answer_exercise_expert(puzzles, resolve, duration, print_hour):
  """--- 14h21 --> 1min tries 5 - Exercise basic of {} solved - [x] of [10] ---
    Question expert
  """
  choice = ''
  tries_puzzle = 5
  count_tries = 0
  #change the boucle for enter word by word the good answer
  while choice not in puzzles[resolve]['solution']:
    
    print_exercise_expert(puzzles[resolve], resolve, tries_puzzle, duration, print_hour)
    choice = input("Your choice: ".rjust(65))

    if choice != '':
      
      if choice.upper() == 'Q' or choice.upper() == 'E' or choice.upper() == 'N':
        return choice, count_tries

      elif choice.upper() != 'Q' and choice.upper() != 'E' and choice.upper() != 'N':
        tries_puzzle -= 1
        count_tries += 1

      if tries_puzzle == 0:
        choice = 'lose'
        return choice, count_tries
    
  return choice, count_tries
##
##
def get_answer_exercise_expert_end(duration, resolve, total_try):
  """The session expert lasted 1min. You solved 2/10 puzzles in 3 tries. ---
    
  """
  choice = ''

  while choice.upper() != 'Q' and choice.upper() != 'M':
    print_exercise_expert_end(duration, resolve, total_try)
    choice = input("Your choice: ".rjust(65))
    # Maybe an option for see directly the current session and look puzzle OR
    #See an package lesson about the methods not find but unsorted.

  return choice
#
#------------------------------------------------
#------------- EXERCISE HARD --------------------
#------------------------------------------------
#
# NEED TO BE UPDATE
#
#----------------------------------------------------------------------------
#--########################################################################--
#--############################# OVERVIEW #################################--
#--########################################################################--
#----------------------------------------------------------------------------
def get_answer_menu_overview():
  """------ Overview of ------
      1 - Find methods      12
      2 - Exercise basic    3
      3 - Exercise expert   42
      4 - Exercise hard     24
      5 - All exercises     
  """
  choice = -1
  while choice < 1 or choice > 2:
    print_menu_overview()

    choice = input("Your choice?: ".rjust(65))

    if choice.upper() == 'Q' or choice.upper() == 'B':
      return choice
    
    else:
      try:
        choice = int(choice)
      except ValueError:
        choice = -1

  return choice
##
#------------------------------------------------
#--------------- FIND METHOD --------------------
#------------------------------------------------
#
def get_answer_menu_overview_method():
  """- - - overview find methods - - - 
            1 - str 
            2 - list 
            ... 
            7 - native fc"""
  choice = -1

  while choice < 1 or choice > 7:
    print_menu_overview_method()

    choice = input("Your choice?: ".rjust(65))

    if choice.upper() == 'Q' or choice.upper() == 'B':
      return choice
    
    else:
      try:
        choice = int(choice)
      except ValueError:
        choice = -1

  return choice
##
##
def get_answer_start_session_overview_method(sessions, type_obj):
  """----------- All sessions of type_obj page [x] of [y] ---------------
  |   1 -- 24-02-2022 11:55 --   7% ||  11 -- 24-02-2022 14:26 --   3% |
  |   2 -- 24-02-2022 13:39 --  13% ||  12 -- 24-02-2022 14:34 --   3% |
  """
  page, max_page, count_page, = sessions[0], len(sessions), 0
  length_page = len(page)

  choice = -1
  while (choice < (count_page * 20 + 1)) or (choice > (count_page * 20 + length_page)): # can be equal to Q, B, Number, >, <, 

    page = sessions[count_page]
    length_page = len(page)
    string_foot = print_start_session_overview_method_header(type_obj, count_page+1, max_page)
    print_start_session_overview_methods(page, length_page, count_page, string_foot)

    choice = input("Your choice: ".rjust(65))

    if choice.upper() == 'Q' or choice.upper() == 'B':
      return choice

    elif choice == '<' and count_page > 0:
        count_page -= 1
        choice = -1
    
    elif choice == '>' and count_page < max_page-1:
        count_page += 1
        choice = -1
    
    else:
      try:
        choice = int(choice)
      except ValueError:
        choice = -1

  return choice
##
##
def get_answer_session_overview_method_focus(number, type_obj, date, time, winrate, total_try, find, not_find):
  """----------- All sessions of type_obj page [x] of [y] ---------------
  Session 1 on the str object on 24-02-2020 11:55. Duration 1min for a winrate of 7%.
                          - - Methods find - -
                          lower() | upper ()
                                ....
  """
  choice = ''
  while choice.upper() != 'Q' and choice.upper() != 'B' and choice.upper() != 'M':
    print_session_overview_method_focus(number, type_obj, date, time, winrate, total_try, find, not_find)
    print("[Q] --> quit | [B] --> back | [M] --> menu".center(120))
    print("\n")
    choice = input("Your choice: ".rjust(65))
  
  return choice
##
##
#------------------------------------------------
#-------------- EXERCISE BASIC ------------------
#------------------------------------------------
#
def get_answer_menu_overview_exercise_basic():
  """- - - overview exercise basic - - - 
            1 - str 
            2 - list 
            ... 
            7 - native fc"""
  choice = -1
  while choice < 1 or choice > 7:
    print_menu_overview_exercise_basic()

    choice = input("Your choice?: ".rjust(65))

    if choice.upper() == 'Q' or choice.upper() == 'B':
      return choice
    
    else:
      try:
        choice = int(choice)
      except ValueError:
        choice = -1

  return choice
##
##
def get_answer_start_session_overview_exercise_basic(sessions, type_obj):
  """----------- All sessions exercise basic of type_obj page [x] of [y] ---------------
  |   1 -- 24-02-2022 11:55 --   70% ||  11 -- 24-02-2022 14:26 --   30% |
  |   2 -- 24-02-2022 13:39 --   10% ||  12 -- 24-02-2022 14:34 --   50% |
  """
  page, max_page, count_page, = sessions[0], len(sessions), 0
  length_page = len(page)

  choice = -1
  while (choice < (count_page * 20 + 1)) or (choice > (count_page * 20 + length_page)): # can be equal to Q, B, Number, >, <, 

    page = sessions[count_page]
    length_page = len(page)
    string_foot = print_start_session_overview_exercise_basic_header(type_obj, count_page+1, max_page)
    print_start_session_overview_exercise_basic(page, length_page, count_page, string_foot, type_obj)

    choice = input("Your choice: ".rjust(65))

    if choice.upper() == 'Q' or choice.upper() == 'B':
      return choice

    elif choice == '<' and count_page > 0:
        count_page -= 1
        choice = -1
    
    elif choice == '>' and count_page < max_page-1:
        count_page += 1
        choice = -1
    
    else:
      try:
        choice = int(choice)
      except ValueError:
        choice = -1

  return choice
##
##
def get_answer_session_overview_exercise_basic_focus(number, type_obj, date, time, winrate, total_try, find, not_find, id_puzzles):

  choice = ''
  while choice.upper() != 'Q' and choice.upper() != 'B' and choice.upper() != 'M' and choice.upper() != 'L':
    print_session_overview_exercise_basic_focus(number, type_obj, date, time, winrate, total_try, find, not_find, id_puzzles)
    print("[Q] --> quit | [B] --> back | [M] --> menu".center(120))
    print("\n")
    choice = input("Your choice: ".rjust(65))
  
  return choice
###########################################################################
############################ G R A P H ####################################
###########################################################################
#
def get_answer_menu_graph():
  """- - - Graph of - - - 
      1 - str        23
      2 - list       4
      ... 
      7 - native fc  12 
  """
  choice = -1
  while choice < 1 or choice > 7:
    print_menu_graph()

    choice = input("Your choice?: ".rjust(65))

    if choice.upper() == 'Q' or choice.upper() == 'B':
      return choice
    
    else:
      try:
        choice = int(choice)
      except ValueError:
        choice = -1

  return choice
##
##
def get_answer_start_menu_graph_focus(type_obj):
  """------Average of sessions by ------
          1 - The winrate
          2 - % time methods is found
          3 - All the tries
          4 - Sessions by day
  """
  choice = -1

  while choice < 1 or choice > 6:
    print_menu_graph_focus(type_obj)

    choice = input("Your choice?: ".rjust(65))

    if choice.upper() == 'Q' or choice.upper() == 'B' or choice.upper() == 'M':
      return choice
    
    else:
      try:
        choice = int(choice)
      except ValueError:
        choice = -1

  return choice
##
##
###########################################################################
############################# LIBRARY #####################################
###########################################################################
#
def get_answer_menu_library():
  """--------- Library ---------
      1 - str
      2- list
      ...
      7 - native fc
  """
  choice = -1
  while choice < 1 or choice > 7:
    print_menu_library()
    choice = input("Your choice?: ".rjust(65))
    if choice.upper() == 'Q' or choice.upper() == 'B':
      return choice
    else:
      try:
        choice = int(choice)
      except ValueError:
        choice = -1

  return choice
##
##
def get_answer_start_library(library, type_obj):
  """----------- Library of the type_obj ---------------
  |   1 - upper()   ||  4 - capitalize() |
  |   2 - lower()   ||  5 - isnumeric()  |
  """
  methods = simplify_methods(type_obj)
  max_value = len(methods)
  

  choice = -1
  while choice < 1 or choice > max_value:

    methods = simplify_methods(type_obj)
    max_value = len(methods)
    index = OBJECTS.index(type_obj)
    string_foot = print_start_library_method_header(type_obj)
    print_start_library_method(methods, string_foot)
    choice = input("Your choice: ".rjust(65))

    if choice.upper() == 'Q' or choice.upper() == 'B' or choice.upper() == 'M':
      return choice, type_obj
    
    elif choice.upper() == '<' and index > 0:
      type_obj = OBJECTS[index-1]
      choice = -1

    elif choice.upper() == '>' and index < len(OBJECTS)-1:
      type_obj = OBJECTS[index+1]
      choice = -1
    
    else:
      try:
        choice = int(choice)
      except ValueError:
        choice = -1

  return choice, type_obj
##
##
def get_answer_library_focus(number, type_obj):
  
  choice = ''
  length = len(TYPE_OF[type_obj])
  while choice.upper() != 'Q' and choice.upper() != 'B' and choice.upper() != 'M':

    print_library_focus(number, type_obj)
    choice = input("Your choice: ".rjust(65))

    if choice.upper() == '>' and (number-1) < length-1:
      number += 1
    
    elif choice.upper() == '<' and (number-1) > 0:
      number -= 1

  return choice
##
##
def get_answer_review_exercise(list,  type_obj):

  index = 0
  choice = ''
  length = len(list)
  while choice.upper() != 'Q' and choice.upper() != 'M' and choice.upper() != 'B':

    print_review_exercise(list[index], type_obj, index, length)

    choice = input("Your choice?: ".rjust(65))

    if choice == '<':
      if index > 0:
        index -= 1
    
    elif choice == '>':
      if index < len(list)-1:
        index += 1

  return choice





  

    

