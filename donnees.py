#-*- coding: utf-8 -*-
#
import random
OBJECTS = ['str', 'list', 'dict', 'set', 'tuple', 'file', 'native fc']
#
PATH_FIND_METHOD = "donnees/donnees_find_"
PATH_EXERCISE = "donnees/donnees_exercise_"
PATH_EXPERT = "donnees/donnees_exercise_expert"
TYPE_OF = {
  'str': [
    'capitalize()', 'casefold()', 'center(width[, fillchar])', 'count(sub[, start[, end]])', 'encode(encoding, errors)',
    'endswith(suffix[, start[, end]])', 'expandtabs(tabsize)','find(sub[, start[, end]])', 'format(*args, **kwargs)',
    'index(sub[, start[, end]])', 'isalnum()', 'isalpha()', 'isdecimal()', 'isdigit()','isidentifier()', 'islower()',
    'isnumeric()', 'join(iterable)', 'lower()', 'partition(sep)', 'replace(old, new[, count])', 'rfind(sub[, start[, end]])',
    'rindex(sub[, start[, end]])', 'split(sep, maxsplit)', 'startswith(prefix[, start[, end]])', 'strip(chars)',
    'swapcase()', 'title()', 'upper()', 'zfill(width)',
  ],
  'list': [
    'append(x)', 'clear()', 'copy()','count(x)', 'extend(iterable)', 'index(x[, start[, end]])', 'insert(i, x)', 'pop(i)', 'remove(x)',
    'reverse()', 'sort(key, reverse)'
  ],
  'dict': [
    'clear()', 'copy()', 'fromkeys(iterable[, value])', 'get(key[, value])', 'items()', 'keys()', 'pop(key[, value])', 'popitem()',
     'setdefault(key[, value])', 'update(iterable)', 'values()',
  ],
  'set': [
    'add(elmnt)', 'clear()', 'copy()', 'difference(set)', 'difference_update(set)', 'discard(elmnt)', 'intersection(set1, set2, ...)',
     'intersection_update(set1, set2, ...)', 'isdisjoint(set)', 'issubset(set)', 'issuperset(set)', 'pop()', 'remove(elmnt)',
      'symmetric_difference(set)', 'symmetric_difference_update(set)', 'union(set1, set2, ...)', 'update(set)',
  ],
  'tuple': [
    'count(x)', 'index(x)',
  ],
  'file': [
    'close()', 'fileno()', 'flush()', 'isatty()','read(size)', 'readable()','readline(size)', 'readlines(hint)','seek(offset)',
    'seekable()','tell()', 'truncate(size)','writable()', 'write(byte)','writelines(list)',
  ],
  'native fc': [
    'abs(x)', 'all(iterable)', 'any(iterable)', 'bin(x)', 'bool(x)', 'chr(i)', 'complex(real[, imag])',
    'delattr(object, name)', 'dict(**kwargs)', 'dir(object)', 'divmod(a, b)', 'enumerate(iterable[, start])', 'filter(function, iterable)',
    'float(x)', 'format(value[, format_spec])', 'frozenset(iterable)', 'getattr(object, name[, default])', 'globals()',
    'hasattr(object, name)', 'help(object)', 'hex(x)', 'id(object)', 'input(prompt)', 'int(x[, base])', 'isinstance(object, classinfo)',
    'issubclass(class, classinfo)', 'iter(object[, sentinel])', 'len(s)', 'list(iterable)', 'locals()', 'map(function, iterable, ...)',
    'max(iterable, *[, key, default])', 'max(*args[, key])', 'min(iterable, *[, key, default])', 'next(iterator[, default])', 'object()', 'oct(x)', 'open(file, mode)',
    'ord(c)', 'pow(base, exp[, mod])', 'print(*objects, ...)', 'range(start, stop[, step])', 'reversed(seq)', 'round(number[, ndigits])',
    'set(iterable)', 'setattr(object, name, value)', 'slice(start, stop[, step])', 'sorted(iterable, *, key, reverse)',
    'sum(iterable[, start])', 'super()', 'tuple(iterable)', 'type(name, bases, dict)', 'vars(object)', 'zip(*iterables)',
  ],
}

EXPERT = {
  '1': {

  },
  '2': {

  },
  '3': {

  },
  '4': {

  },
  '5': {

  },
  '6': {

  },
  '7': {

  },
  '8': {

  },
  '9': {

  },
  '10': {

  },
  '11': {

  },
  '12': {

  },
  
}

BASIC = {
  'str': {
    'capitalize': [
      {
        'id': 1,
        'question': [">> text = 'i love eat a peach.'", ">> x = text._____()", ">> print(x)", ">> I love eat a peach."],
        'solution':['capitalize'],
        'count': 0,
      },
      {
        'id': 2,
        'question': [">> text = 'what is your name?'", ">> x = text._____()", ">> print(x)", ">> What is your name?"],
        'solution':['capitalize'],
        'count': 0
      },
    ],
    'casefold': [
      {
        'id': 3,
        'question': [">> string_1 = 'der fluß'", ">> string_2 = 'der fluss'", ">> x = string_1._____() == string2._____()", ">> print(x)", ">> True"],
        'solution':["casefold"],
        'count': 0,
      },
      {
        'id': 4,
        'question': [">> text = 'I HATE BANANA'", ">> x = text._____()", ">> print(x)", ">> i hate banana"],
        'solution':["casefold", "lower"],
        'count': 0
      },
    ],
    'center': [
      {
        'id': 5,
        'question': [">> text = 'hello'", ">> x = text._____(6)", ">> print(x)", ">>    hello   "],
        'solution':["center"],
        'count': 0,
      },
      {
        'id': 6,
        'question': [">> text = 'choice'", ">> x = text._____(11, '*')", ">> print(x)", ">> *****choice******"],
        'solution':["center"],
        'count': 0
      },
    ],
    'count': [
      {
        'id': 7,
        'question': [">> text = 'Do you like? You eat your fruits.'", ">> x = text._____('you')", ">> print(x)", ">> 3"],
        'solution':["count"],
        'count': 0,
      },
      {
        'id': 8,
        'question': [">> text = 'Sometime, i remember'", ">> x = text._____('e')", ">> print(x)", ">> 5"],
        'solution':["count"],
        'count': 0
      },
    ],
    'encode': [
      {
        'id': 9,
        'question': [">> text = 'mañana = matin'", ">> x = text._____()", ">> print(x)", ">> b'ma\\xc3\\xb1ana = matin'"],
        'solution':["encode"],
        'count': 0,
      },
      {
        'id': 10,
        'question': [">> name = 'Ðaniel'", ">> x = name._____('ascii','ignore')", ">> print(x)", ">> b'aniel'"],
        'solution':["encode"],
        'count': 0
      },
    ],
    'endswith': [
      {
        'id': 11,
        'question': [">> text = 'My name is Jack.'", ">> x = text._____('Jack')", ">> print(x)", ">> True"],
        'solution':["endswith"],
        'count': 0,
      },
      {
        'id': 12,
        'question': [">> text = 'apple and orange'", ">> x = text._____('and', 0, 9)", ">> print(x)", ">> True"],
        'solution':["endswith"],
        'count': 0
      },
    ],
    'expandtabs': [
      {
        'id': 13,
        'question': [">> text = 'Damien\\tand\\tDavid.'", ">> x = text._____()", ">> print(x)", ">> Damien  and    David"],
        'solution':["expandtabs"],
        'count': 0,
      },
      {
        'id': 14,
        'question': [">> text = 'abc\\tdefg\\thi'", ">> x = text._____(__)", ">> print(x)", ">> abc   defg  hi"],
        'solution':["expandtabs, 3", "expandtabs,3"],
        'count': 0
      },
    ],
    'find': [
      {
        'id': 15,
        'question': [">> text = 'A coffee pleas!:'", ">> x = text._____('coffee')", ">> print(x)", ">> 2"],
        'solution':["find", "index"],
        'count': 0,
      },
      {
        'id': 16,
        'question': [">> text = 'I hate banana.'", ">> x = text._____('kiwi')", ">> print(x)", ">> -1"],
        'solution':["find"],
        'count': 0
      },
    ],
    'format': [
      {
        'id': 17,
        'question': [">> text = 'I sold a {} for {} dollars.'", ">> x = text._____('skate', 11)", ">> print(x)", ">> I sold a skate for 11 dollars."],
        'solution':["format"],
        'count': 0,
      },
      {
        'id': 18,
        'question': [">> text = 'I eat an {1} and a {0}.'", ">> x = text._____( 'peach', 'apple')", ">> print(x)", ">> I eat an apple and a peach."],
        'solution':["format"],
        'count': 0
      },
    ],
    'index': [
      {
        'id': 19,
        'question': [">> text = 'Maybe tomorrow'", ">> x = text._____(__, 2, 9)", ">> print(x)", ">> 6"],
        'solution':["count, 't'", "index, 't'", "count,'t'", "index,'t'"],
        'count': 0,
      },
      {
        'id': 20,
        'question': [">> text = 'My name is Jack.'", ">> x = text._____('Jean')", ">> print(x)", ">> ValueError: substring not found"],
        'solution':["index"],
        'count': 0
      },
    ],
    'isalnum': [
      {
        'id': 21,
        'question': [">> text = 'sectorA2B9'", ">> x = text._____()", ">> print(x)", ">> True"],
        'solution':["isalnum", "isidentifier"],
        'count': 0,
      },
      {
        'id': 22,
        'question': [">> pseudo = 'alpha123'", ">> x = pseudo._____()", ">> print(x)", ">> True"],
        'solution':["isalnum", "isidentifier"],
        'count': 0
      },
    ],
    'isalpha': [
      {
        'id': 23,
        'question': [">> text = 'roadS'", ">> x = text._____()", ">> print(x)", ">> True"],
        'solution':["isalnum", "isidentifier", "isalpha"],
        'count': 0,
      },
      {
        'id': 24,
        'question': [">> name = 'Baptiste'", ">> x = name._____()", ">> print(x)", ">> True"],
        'solution':["isalnum", "isidentifier", "isalpha"],
        'count': 0
      },
    ],
    'isdecimal': [
      {
        'id': 25,
        'question': [">> a = '\\u0030'", ">> x = a._____()", ">> print(x)", ">> True"],
        'solution':["isdecimal", "isdigit", "isalnum", "isnumeric"],
        'count': 0,
      },
      {
        'id': 26,
        'question': [">> a = '\\u0038'", ">> x = a._____()", ">> print(x)", ">> True"],
        'solution':["isdecimal", "isdigit", "isalnum", "isnumeric"],
        'count': 0
      },
    ],
    'isdigit': [
      {
        'id': 27,
        'question': [">> a = '\\u00B2'", ">> x = a._____()", ">> print(x)", ">> True"],
        'solution':["isdigit", "isnumeric", "isalnum"],
        'count': 0,
      },
      {
        'id': 28,
        'question': [">> a = '\\u00B3'", ">> x = a._____()", ">> print(x)", ">> True"],
        'solution':["isdigit", "isnumeric", "isalnum"],
        'count': 0
      },
    ],
    'isidentifier': [
      {
        'id': 29,
        'question': [">> pseudo = 'joker_123'", ">> x = pseudo._____()", ">> print(x)", ">> True"],
        'solution':["isidentifier"],
        'count': 0,
      },
      {
        'id': 30,
        'question': [">> name = '__Max__'", ">> x = name._____()", ">> print(x)", ">> True"],
        'solution':["isidentifier"],
        'count': 0
      },
    ],
    'islower': [
      {
        'id': 31,
        'question': [">> text = 'you have 3 cats.'", ">> x = text._____()", ">> print(x)", ">> True"],
        'solution':["islower"],
        'count': 0,
      },
      {
        'id': 32,
        'question': [">> text = 'hello my friend!'", ">> x = text._____()", ">> print(x)", ">> True"],
        'solution':["islower"],
        'count': 0
      },
    ],
    'isnumeric': [
      {
        'id': 33,
        'question': [">> a = '\\u00BD'", ">> x = a._____()", ">> print(x)", ">> True"],
        'solution':["isnumeric", "isalnum"],
        'count': 0,
      },
      {
        'id': 34,
        'question': [">> a = '\\u00B23455'", ">> x = a._____()", ">> print(x)", ">> True"],
        'solution':["isnumeric", "isalnum", "isdigit"],
        'count': 0
      },
    ],
    'join': [
      {
        'id': 35,
        'question': [">> fruits = ('peach', 'cherry', 'kiwi')", ">> x = ' or '._____(fruits)", ">> print(x)", ">> peach or cherry or kiwi"],
        'solution':["join"],
        'count': 0,
      },
      {
        'id': 36,
        'question': [">> person = {'name': 'Patrick', 'age': 43, 'country': 'Spanish'}", ">> x = ' | '._____(person)", ">> print(x)", ">> name | age | country"],
        'solution':["join"],
        'count': 0
      },
    ],
    'lower': [
      {
        'id': 37,
        'question': [">> text = 'Yes! I win'", ">> x = text._____()", ">> print(x)", ">> yes! i win"],
        'solution':["casefold", "lower"],
        'count': 0,
      },
      {
        'id': 38,
        'question': [">> text = 'I have 19 Books.'", ">> x = text._____()", ">> print(x)", ">> i have 19 books."],
        'solution':["casefold", "lower"],
        'count': 0
      },
    ],
    'partition': [
      {
        'id': 39,
        'question': [">> text = 'I walk in the forest.'", ">> x = text._____('in')", ">> print(x)", ">> ('I walk ', 'in', ' the forest.')"],
        'solution':["partition"],
        'count': 0,
      },
      {
        'id': 40,
        'question': [">> text = 'Yesterday you drove.'", ">> x = text._____('you')", ">> print(x)", ">> ('Yesterday ', 'you', ' drove.')"],
        'solution':["partition"],
        'count': 0
      },
    ],
    'replace': [
      {
        'id': 41,
        'question': [">> text = 'I climb a tree.'", ">> x = text._____('tree', 'wall')", ">> print(x)", ">> I climb a wall."],
        'solution':["replace"],
        'count': 0,
      },
      {
        'id': 42,
        'question': [">> text = 'I have 2 dogs, 2 cats, 2 cows.'", ">> x = text._____('2', '7', __)", ">> print(x)", ">> I have 7 dogs, 7 cats, 2 cows."],
        'solution':["replace, 2", "replace,2"],
        'count': 0
      },
    ],
    'rfind': [
      {
        'id': 43,
        'question': [">> text = 'I wear glasses.'", ">> x = text._____('e')", ">> print(x)", ">> 12"],
        'solution':["rfind", "rindex"],
        'count': 0,
      },
      {
        'id': 44,
        'question': [">> text = 'Do you need help?'", ">> x = text._____('e', 2, 10)", ">> print(x)", ">> 9"],
        'solution':["rfind", "rindex"],
        'count': 0
      },
    ],
    'rindex': [
      {
        'id': 45,
        'question': [">> text = 'I drink a coffee.'", ">> x = text._____('drink', 0, 15)", ">> print(x)", ">> 2"],
        'solution':["rfind", "rindex", "find", "index"],
        'count': 0,
      },
      {
        'id': 46,
        'question': [">> text = 'She eat an apple.'", ">> x = text._____('peach')", ">> ValueError: substring not found"],
        'solution':["rindex", "index"],
        'count': 0
      },
    ],
    'split': [
      {
        'id': 47,
        'question': [">> text = '3, 2, 1, go!'", ">> x = text._____(' ')", ">> print(x)", ">> ['3', '2', '1', 'go']"],
        'solution':["split"],
        'count': 0,
      },
      {
        'id': 48,
        'question': [">> text = '#python#react#code#job'", ">> x = text._____('#', 3)", ">> print(x)", ">> ['', 'python', 'react', 'code#job']"],
        'solution':["split"],
        'count': 0
      },
    ],
    'startswith': [
      {
        'id': 49,
        'question': [">> text = 'I cook 3 eggs.'", ">> x = text._____('cook', 2, 12)", ">> print(x)", ">> True"],
        'solution':["startswith"],
        'count': 0,
      },
      {
        'id': 50,
        'question': [">> text = 'cat versus dog'", ">> x = text._____('cat')", ">> print(x)", ">> True"],
        'solution':["startswith"],
        'count': 0
      },
    ],
    'strip': [
      {
        'id': 51,
        'question': [">> text = 'mmhmhm...I'm thinking...'", ">> x = text._____('m. h')", ">> print(x)", ">> I'm thinking"],
        'solution':["strip"],
        'count': 0,
      },
      {
        'id': 52,
        'question': [">> text = '... he...she knows...'", ">> x = text._____('.')", ">> print(x)", ">> he...she knows"],
        'solution':["strip"],
        'count': 0
      },
    ],
    'swapcase': [
      {
        'id': 53,
        'question': [">> text = 'What do you mean?'", ">> x = text._____()", ">> print(x)", ">> wHAT DO YOU MEAN?"],
        'solution':["swapcase"],
        'count': 0,
      },
      {
        'id': 54,
        'question': [">> text = 'YEAH! I finish first!'", ">> x = text._____()", ">> print(x)", ">> yeah! i FINISH FIRST!"],
        'solution':["swapcase"],
        'count': 0
      },
    ],
    'title': [
      {
        'id': 55,
        'question': [">> text = '81poker is my PSEUDO.'", ">> x = text._____()", ">> print(x)", ">> 81Poker Is My Pseudo."],
        'solution':["title"],
        'count': 0,
      },
      {
        'id': 56,
        'question': [">> text = 'I go to school.'", ">> x = text._____()", ">> print(x)", ">> I Go To School."],
        'solution':["title"],
        'count': 0
      },
    ],
    'upper': [
      {
        'id': 57,
        'question': [">> text = 'Are you sure George?'", ">> x = text._____()", ">> print(x)", ">> ARE YOU SURE GEORGE?"],
        'solution':["upper"],
        'count': 0,
      },
      {
        'id': 58,
        'question': [">> text = '--51Zone--'", ">> x = text._____(')", ">> print(x)", ">> --51ZONE--"],
        'solution':["upper"],
        'count': 0
      },
    ],
    'zfill': [
      {
        'id': 59,
        'question': [">> text = '123456789'", ">> x = text._____(10)", ">> print(x)", ">> 0123456789"],
        'solution':["zfill"],
        'count': 0,
      },
      {
        'id': 60,
        'question': [">> text = '% to win'", ">> x = text._____(__)", ">> print(x)", ">> 0% to win"],
        'solution':["zfill,9", "zfill, 9"],
        'count': 0
      },
    ],
  },
  'list': {
    'append': [
      {
        'id': 1,
        'question': [">> fruits = ['apple', 'banana', 'peach']", ">> fruits._____('kiwi')", ">> ['apple', 'banana', 'peach', 'kiwi']"],
        'solution':["append"],
        'count': 0,
      },
      {
        'id': 2,
        'question': [">> arr = [1, 2, 3]", ">> arr._____({'four': 4, 'five': 5, 'six': 6})", ">> [1, 2, 3, {'four': 4, 'five': 5, 'six': 6}]"],
        'solution':["append"],
        'count': 0
      },
    ],
    'clear': [
      {
        'id': 3,
        'question': [">> arr = [1, 2, 3, 4, 5]", ">> arr._____()", ">> print(arr)", ">> []"],
        'solution':["clear"],
        'count': 0,
      },
      {
        'id': 4,
        'question': [">> name = ['Hugo', 'Julia', 'Robert']", ">> name._____()", ">> print(name)", ">> []"],
        'solution':["clear"],
        'count': 0
      },
    ],
    'copy': [
      {
        'id': 5,
        'question': [">> arr = ['hey', 51, '...']", ">> x = arr._____()", ">> print(x)", ">> ['hey', 51, '...']"],
        'solution':["copy"],
        'count': 0,
      },
      {
        'id': 6,
        'question': [">> arr = [{'age': 25, 'sex': 'man'}, 'home']", ">> x = arr._____()", ">> print(x)", ">> [{'age': 25, 'sex': 'man'}, 'home']"],
        'solution':["copy"],
        'count': 0
      },
    ],
    'count': [
      {
        'id': 7,
        'question': [">> arr = [(1, 2, 3), 1, 2, 3]", ">> x = arr._____(1)", ">> print(x)", ">> 1"],
        'solution':["count"],
        'count': 0,
      },
      {
        'id': 8,
        'question': [">> arr = ['aa', 'ab', 'ba', 'ab', 'bb']", ">> x = arr._____('ab')", ">> print(x)", ">> 2"],
        'solution':["count"],
        'count': 0
      },
    ],
    'extend': [
      {
        'id': 9,
        'question': [">> prime = [2, 3, 5, 7]", ">> prime._____([11, 13, 17, 19])", ">> print(prime)", ">> [2, 3, 5, 7, 11, 13, 17, 19]"],
        'solution':["extend"],
        'count': 0,
      },
      {
        'id': 10,
        'question': [">> list = [('a', 'a'), ('a', 'b')]", ">> list._____([('b', 'a'), ('b', 'b')])", ">> print(list)", ">> [('a', 'a'), ('a', 'b'), ('b', 'a'), ('b', 'b')]"],
        'solution':["extend"],
        'count': 0
      },
    ],
    'index': [
      {
        'id': 11,
        'question': [">> arr = ['a', 'c', 'b', 'd', 'f', 'e']", ">> x = arr._____('b')", ">> print(x)", ">> 2"],
        'solution':["index"],
        'count': 0,
      },
      {
        'id': 12,
        'question': [">> arr = [3, 7, 9, 1, 7, 4, 2, 6]", ">> x = arr._____(7, 2, -2)", ">> print(x)", ">> 4"],
        'solution':["index"],
        'count': 0
      },
    ],
    'insert': [
      {
        'id': 13,
        'question': [">> arr = [2, 4, 8]", ">> arr._____(-1, 6)", ">> print(arr)", ">> [2, 4, 6, 8]"],
        'solution':["insert"],
        'count': 0,
      },
      {
        'id': 14,
        'question': [">> name = ['Mathieu', 'Jean', 'Léa']", ">> name._____(1, 'Léo')", ">> print(name)", ">> ['Mathieu', 'Jean', 'Léa', 'Léo']"],
        'solution':["insert"],
        'count': 0
      },
    ],
    'pop': [
      {
        'id': 15,
        'question': [">> prime = [2, 3, 5, 7, 10, 11]", ">> x = prime._____(-1)", ">> print(prime)", ">> [2, 3, 5, 7, 11]", ">> print(x)", ">> 10"],
        'solution':["pop"],
        'count': 0,
      },
      {
        'id': 16,
        'question': [">> name = ['Alice', 'Zoé', 'Kévin', 'dog']", ">> x = name._____()", ">> print(name)", ">> ['Alice', 'Zoé', 'Kévin']", ">> print(x)", ">> dog"],
        'solution':["pop"],
        'count': 0
      },
    ],
    'remove': [
      {
        'id': 17,
        'question': [">> arr = ['good', 'good', 'error', 'good']", ">> arr._____('error')", ">> print(arr)", ">> ['good', 'good', 'good']"],
        'solution':["remove"],
        'count': 0,
      },
      {
        'id': 18,
        'question': [">> arr = [1, 2, 3, 'four', 5, 6]", ">> arr._____('four')", ">> print(arr)", ">> [1, 2, 3, 5, 6]"],
        'solution':["remove"],
        'count': 0
      },
    ],
    'reverse': [
      {
        'id': 19,
        'question': [">> arr = ['D', 'O', 'G']", ">> arr._____()", ">> print(arr)", ">> ['G', 'O', 'D']"],
        'solution':["reverse"],
        'count': 0,
      },
      {
        'id': 20,
        'question': [">> [(7, 'a'), '??', '...', 8]", ">> arr._____()", ">> print(arr)", ">> [8, '...', '??', (7, 'a')]"],
        'solution':["reverse"],
        'count': 0
      },
    ],
    'sort': [
      {
        'id': 21,
        'question': [">> name = ['Chloé', 'Zoé', 'Alex', 'Céline']", ">> def length(e):", ">>   return len(e)", ">> name._____(key=length, reverse=True)", ">> print(name)",
        ">> ['Céline', 'Chloé', 'Alex', 'Zoé'"],
        'solution':["sort"],
        'count': 0,
      },
      {
        'id': 22,
        'question': [">> arr = [5, 7, 12, 1, 3, 8, 0, 55]", ">> arr._____()", ">> print(arr)", ">> [0, 1, 3, 5, 7, 8, 12, 55]"],
        'solution':["sort"],
        'count': 0
      },
    ],
  },
  'dict': {
    'clear': [
      {
        'id': 1,
        'question': [">> dict = {'a': 0, 'b': 1}", ">> dict._____()", ">> print(dict)", ">> {}"],
        'solution': ["clear"],
        'count': 0,
      },
      {
        'id': 2,
        'question': [">> person = {'name': 'Léo', 'age': 18}", ">> person._____()", ">> print(person)", ">> {}"],
        'solution': ["clear"],
        'count': 0
      },
    ],
    'copy': [
      {
        'id': 3,
        'question': [">> dict = {'car': 23, 'bike': 47}", ">> vehicle = dict._____()", ">> print(vehicle)", ">> {'car': 23, 'bike': 47}"],
        'solution': ["copy"],
        'count': 0,
      },
      {
        'id': 4,
        'question': [">> dict = {'life': 3, 'xp': 258}", ">> game = dict._____()", ">> print(game)", ">> {'life': 3, 'xp': 258}"],
        'solution': ["copy"],
        'count': 0
      },
    ],
    'fromkeys': [
      {
        'id': 5,
        'question': [">> key = ('a', 'b', 'c', 'd')", ">> dict = dict._____(key, 0)", ">> print(dict)", ">> {'a': 0, 'b': 0 ,'c': 0, 'd': 0}"],
        'solution': ["fromkeys"],
        'count': 0,
      },
      {
        'id': 6,
        'question': [">> key = [0, 1, 2, 3, 4]", ">> dict = dict._____(key)", ">> print(dict)", ">> {0: None, 1: None, 2: None, 3: None, 4: None}"],
        'solution': ["fromkeys"],
        'count': 0
      },
    ],
    'get': [
      {
        'id': 7,
        'question': [">> person = {'name': 'Théo', 'age': 18}", ">> x = person._____('sex', 'error')", ">> print(x)", ">> error"],
        'solution': ["get"],
        'count': 0,
      },
      {
        'id': 8,
        'question': [">> person = {'name': 'Alex', 'age': 25}", ">> x = person._____('name')", ">> print(x)", ">> Alex"],
        'solution': ["get"],
        'count': 0
      },
    ],
    'items': [
      {
        'id': 9,
        'question': [">> vehicle = {'bus': 3, 'car': 62}", ">> x = vehicle._____()", ">> print(x)", ">> dict______([('bus', 3), ('car', 62)])"],
        'solution': ["items"],
        'count': 0,
      },
      {
        'id': 10,
        'question': [">> dict = {'a': 1, 'b': 2}", ">> x = dict._____()", ">> print(x)", ">> dict______([('a', 1), ('b', 2)])"],
        'solution': ["items"],
        'count': 0
      },
    ],
    'keys': [
      {
        'id': 11,
        'question': [">> person = {'name': 'Léa', 'age': 23, 'sex': 'woman'}", ">> x = person._____()", ">> print(x)", ">> dict______(['name', 'age', 'sex'])"],
        'solution': ["keys"],
        'count': 0,
      },
      {
        'id': 12,
        'question': [">> dict = {'a': 0, 'b': 1, 'c': 2}", ">> x = dict._____()", ">> print(x)", ">> dict______(['a', 'b', 'c'])"],
        'solution': ["keys"],
        'count': 0
      },
    ],
    'pop': [
      {
        'id': 13,
        'question': [">> dict = {'en': 'Hello', 'fr': 'Bonjour'}", ">> x = dict._____('fr')", ">> print(dict)", ">> {'en': 'Hello'}", ">> print(x)", ">> Bonjour"],
        'solution': ["pop"],
        'count': 0,
      },
      {
        'id': 14,
        'question': [">> dict = {'a': 1, 'b': 2, 'c': 3}", ">> x = dict._____('d', 4)", ">> print(dict)", ">> {'a': 1, 'b': 2, 'c': 3}", ">> print(x)", ">> 4"],
        'solution': ["pop"],
        'count': 0
      },
    ],
    'popitem': [
      {
        'id': 15,
        'question': [">> vehicle = {'car': 12, 'bus': 5}", ">> x = vehicle._____()", ">> print(vehicle)", ">> {'car': 12}", "print(x)", ">> ('bus', 5)"],
        'solution': ["popitem"],
        'count': 0,
      },
      {
        'id': 16,
        'question': [">> dict = {'a': 0, 'b': 1, 'c': 2}", ">> x = dict._____()", ">> print(dict)", ">> {'a': 0, 'b': 1}", ">> print(x)", ">> ('c': 2)"],
        'solution': ["popitem"],
        'count': 0
      },
    ],
    'setdefault': [
      {
        'id': 17,
        'question': [">> fruits = {'apple': 7, 'kiwi': 13}", ">> x = fruits._____('kiwi')", ">> print(fruits)", ">> {'apple': 7, 'kiwi': 13}", ">> print(x)",
        ">> 13"],
        'solution': ["setdefault"],
        'count': 0,
      },
      {
        'id': 18,
        'question': [">> fruits = {'peach': 5, 'banana': 9}", ">> x = fruits._____('cherry', 4)", ">> print(fruits)", ">> {'peach': 5, 'banana': 9, 'cherry': 4}",
        ">> print(x)", ">> 4"],
        'solution': ["setdefault"],
        'count': 0
      },
    ],
    'update': [
      {
        'id': 19,
        'question': [">> person = {'name': 'Léo', 'age': 32}", ">> person._____({'sex': 'man'})", ">> print(person)", ">> {'name': 'Léo', 'age': 32, 'sex': 'man'}"],
        'solution': ["update"],
        'count': 0,
      },
      {
        'id': 20,
        'question': [">> dict = {'a': 1, 'b': 2, 'c': 3}", ">> dict._____(c = 4, d = 5)", ">> print(dict)", ">> {'a': 1, 'b': 2, 'c': 4, 'd': 5}"],
        'solution': ["update"],
        'count': 0
      },
    ],
    'values': [
      {
        'id': 21,
        'question': [">> vehicle = {'brand': 'Ford', 'color': 'black', 'year': 1972}", ">> x = vehicle._____()", ">> print(x)",
        ">> dict______(['Ford', 'black', 1972])"],
        'solution': ["values"],
        'count': 0,
      },
      {
        'id': 22,
        'question': [">> dict = {1: 'one', 2: 'two', 3: 'three', 4: 'four'}", ">> x = dict._____()", ">> dict[2] = 'error'", ">> print(x)", 
        ">> dict______(['one', 'error', 'three', 'four'])"],
        'solution': ["values"],
        'count': 0
      },
    ],
  },
  'set': {
    'add': [
      {
        'id': 1,
        'question': [">> animal = {'cat', 'dog', 'sheep'}", ">> animal._____('cow')", ">> print(animal)", ">> {'cat', 'dog', 'sheep', 'cow'}"],
        'solution': ["add"],
        'count': 0,
      },
      {
        'id': 2,
        'question': [">> set = {5, 7, 3, 1, 9}", ">> set._____(3)", ">> print(set)", ">> {5, 3, 1, 7, 9}"],
        'solution': ["add"],
        'count': 0
      },
    ],
    'clear': [
      {
        'id': 3,
        'question': [">> fruits = {'apple', 'kiwi', 'cherry'}", ">> fruits._____()", ">> print(fruits)", ">> set()"],
        'solution': ["clear"],
        'count': 0,
      },
      {
        'id': 4,
        'question': [">> set = {5, 1, 3, 4, 9}", ">> set._____()", ">> print(set)", ">> set()"],
        'solution': ["clear"],
        'count': 0
      },
    ],
    'copy': [
      {
        'id': 5,
        'question': [">> vehicle = {'moto', 'bus', 'car'}", ">> x = vehicle._____()", ">> print(x)", ">> {'car', 'bus', 'moto'}"],
        'solution': ["copy"],
        'count': 0,
      },
      {
        'id': 6,
        'question': [">> set = {5, 3, 7, 4}", ">> x = set._____()", ">> print(x)", ">> {7, 5, 4, 3}"],
        'solution': ["copy"],
        'count': 0
      },
    ],
    'difference': [
      {
        'id': 7,
        'question': [">> a = {'Léa', 'Céline', 'Kévin'}", ">> b = {'Sarah', 'Léa', 'Céline'}", ">> x = a._____(b)", ">> print(x)", ">> {'Kévin'}"],
        'solution': ["difference"],
        'count': 0,
      },
      {
        'id': 8,
        'question': [">> a = {2, 3, 11, 7}", ">> b = {3, 5, 13, 2}", ">> x = b._____(a)", ">> print(x)", ">> {13, 5}"],
        'solution': ["difference"],
        'count': 0
      },
    ],
    'difference_update': [
      {
        'id': 9,
        'question': [">> a = {'sheep', 'dog', 'cat'}", ">> b = {'cow', 'dog', 'pig'}", ">> a._____(b)", ">> print(a)", ">> {'cat', 'sheep'}"],
        'solution': ["difference_update"],
        'count': 0,
      },
      {
        'id': 10,
        'question': [">> a = {5, 7, 3, 2, 9}", ">> b = {8, 2, 3, 1}", ">> b._____(a)", ">> print(b)", ">> {1, 8}"],
        'solution': ["difference_update"],
        'count': 0
      },
    ],
    'discard': [
      {
        'id': 11,
        'question': [">> table = {'fork', 'plate', 'stove'}", ">> table._____('stove')", ">> print(table)", ">> {'plate', 'fork'}"],
        'solution': ["discard"],
        'count': 0,
      },
      {
        'id': 12,
        'question': [">> set = {3, 5, 7, 4}", ">> set._____(7)", ">> print(set)", ">> {4, 3, 5}"],
        'solution': ["discard"],
        'count': 0
      },
    ],
    'intersection': [
      {
        'id': 13,
        'question': [">> fruits = {'apple', 'kiwi', 'peach'}", ">> word = {'table', 'peach', 'fork'}", ">> x = fruits._____(word)", ">> print(x)", ">> {'peach'}"],
        'solution': ["intersection"],
        'count': 0,
      },
      {
        'id': 14,
        'question': [">> a = {7, 5, 3, 1}", ">> b = {3, 9, 2, 4}", ">> c = {2, 8, 3, 9}", ">> x = a._____(b, c)", ">> print(x)", ">> {3}"],
        'solution': ["intersection"],
        'count': 0
      },
    ],
    'intersection_update': [
      {
        'id': 15,
        'question': [">> vehicle = {'moto', 'bus', 'car'}", ">> word = {'road', 'bus', 'man'}", ">> vehicle.____(word)", ">> print(x)", ">> {'bus'}"],
        'solution': ["intersection_update"],
        'count': 0,
      },
      {
        'id': 16,
        'question': [">> set1 = {'a', 'b', 'c', 'd'}", ">> set2 = {'c', 'f', 'd', 'g'}", ">> set3 = {'z', 't', 'd', 'c'}", ">> set1._____(set2, set3)",
        ">> print(set1)", ">> {'d', 'c'}"],
        'solution': ["intersection_update"],
        'count': 0
      },
    ],
    'isdisjoint': [
      {
        'id': 17,
        'question': [">> a = {'kiwi', 'man', 'moto'}", ">> b = {'desk', 'number', 'fork'}", ">> x = a._____(b)", ">> print(x)", ">> True"],
        'solution': ["isdisjoint"],
        'count': 0,
      },
      {
        'id': 18,
        'question': [">> a = {'girl', 'name', 'door'}", ">> b = {'smile', 'bow', 'phone'}", ">> x = a._____(b)", ">> print(x)", ">> True"],
        'solution': ["isdisjoint"],
        'count': 0
      },
    ],
    'issubset': [
      {
        'id': 19,
        'question': [">> a = {'b', 'd', 'f'}", ">> b = {'a', 'f', 'z', 'd', 'e', 'b'}", ">> x = a._____(b)", ">> print(x)", ">> True"],
        'solution': ["issubset"],
        'count': 0,
      },
      {
        'id': 20,
        'question': [">> a = {'z', 'y', 'x'}", ">> b = {'a', 'y', 'z', 'g'}", ">> x = a._____(b)", ">> print(x)", ">> False"],
        'solution': ["issubset", "isdisjoint"],
        'count': 0
      },
    ],
    'issuperset': [
      {
        'id': 21,
        'question': [">> a = {'hi', 'hello', 'hola', 'bonjour'}", ">> b = {'hi', 'hola'}", ">> x = a._____(b)", ">> print(x)", ">> True"],
        'solution': ["issuperset"],
        'count': 0,
      },
      {
        'id': 22,
        'question': [">> a = {'b', 'd', 'i', 'e', 't'}", ">> b = {'d', 'e', 'u'}", ">> x = a._____(b)", ">> print(x)", ">> False"],
        'solution': ["issubset", "isdisjoint", "issuperset"],
        'count': 0
      },
    ],
    'pop': [
      {
        'id': 23,
        'question': [">> word = {'fork', 'bus', 'man', 'dog'}", ">> x = word._____()", ">> print(word)", ">> {'man', 'dog', 'bus'}", ">> print(x)", ">> fork"],
        'solution': ["pop"],
        'count': 0,
      },
      {
        'id': 24,
        'question': [">> set = {'a', 'b', 'c', 'd'}", ">> set._____()", ">> print(set)", ">> {'a', 'b', 'c'}"],
        'solution': ["pop"],
        'count': 0
      },
    ],
    'remove': [
      {
        'id': 25,
        'question': [">> name = {'Alex', 'John', 'Cédric'}", ">> name._____('John')", ">> print(name)", ">> {'Cédric, 'Alex'}"],
        'solution': ["remove"],
        'count': 0,
      },
      {
        'id': 26,
        'question': [">> fruits = {'cherry', 'kiwi', 'peach'}", ">> fruits._____('apple')", ">> KeyError: 'apple'"],
        'solution': ["remove"],
        'count': 0
      },
    ],
    'symmetric_difference': [
      {
        'id': 27,
        'question': [">> a = {'banana', 'house', 'knife'}", ">> b = {'dog', 'car', 'house'}", ">> x = a._____(b)", ">> print(x)", ">> {'knife', 'dog', 'banana', 'car'}"],
        'solution': ["symmetric_difference"],
        'count': 0,
      },
      {
        'id': 28,
        'question': [">> a = {5, 7, 8, 2, 1}", ">> b = {3, 5, 9, 7, 2}", ">> x = a._____(b)", ">> print(x)", ">> {3, 9, 1, 8}"],
        'solution': ["symmetric_difference"],
        'count': 0
      },
    ],
    'symmetric_difference_update': [
      {
        'id': 29,
        'question': [">> a = {'tree', 'cow', 'desk'}", ">> b = {'bus', 'fork', 'cow'}", ">> a._____(b)", ">> print(a)", ">> {'desk', 'bus', 'tree', 'fork'}"],
        'solution': ["symmetric_difference_update"],
        'count': 0,
      },
      {
        'id': 30,
        'question': [">> a = {3, 7, 5, 2, 1}", ">> b = {9, 3, 8, 5, 4}", ">> a._____(b)", ">> print(a)", ">> {7, 8, 4, 2, 9, 1}"],
        'solution': ["symmetric_difference_update"],
        'count': 0
      },
    ],
    'union': [
      {
        'id': 31,
        'question': [">> a = {'Léa', 'Max', 'Aline'}", ">> b = {'Jul', 'Aline', 'Robert'}", ">> x = a._____(b)", ">> print(x)", ">> {'Max', 'Léa', 'Robert', 'Aline', 'Jul'}"],
        'solution': ["union"],
        'count': 0,
      },
      {
        'id': 32,
        'question': [">> a = {3, 5, 8, 12, 15}", ">> b = {1, 3, 9, 12, 0}", ">> c = {5, 15, 2, 4, 12}", ">> x = a._____(b, c)", ">> print(x)",
        ">> {5, 3, 0, 4, 15, 1, 2, 8, 9, 12}"],
        'solution': ["union"],
        'count': 0
      },
    ],
    'update': [
      {
        'id': 33,
        'question': [">> a = {'moto', 'knife', 'man'}", ">> b = {'cow', 'knife', 'moto'}", ">> a._____(b)", ">> print(a)", ">> {'man', 'cow', 'knife', 'moto'}"],
        'solution': ["update"],
        'count': 0,
      },
      {
        'id': 34,
        'question': [">> a = {5, 3, 2, 1}", ">> b = {9, 5, 1, 4}", ">> b._____(a)", ">> print(b)", ">> {5, 3, 1, 4, 9, 2}"],
        'solution': ["update"],
        'count': 0
      },
    ],
  },
  'tuple': {
    'count': [
      {
        'id': 1,
        'question': [">> a = ('hi', 'hello', 'hi', 'bonjour')", ">> x = a._____('hi')", ">> print(x)", ">> 2"],
        'solution': ["count"],
        'count': 0,
      },
      {
        'id': 2,
        'question': [">> a = (3, 2, 3, 4, 2, 3, 5)", ">> x = a._____(3)", ">> print(x)", ">> 3"],
        'solution': ["count"],
        'count': 0
      },
    ],
    'index': [
      {
        'id': 3,
        'question': [">> a = ('car', 'plate', 'cow', 'bus')", ">> x = a._____('plate')", ">> print(x)", ">> 1"],
        'solution': ["index"],
        'count': 0,
      },
      {
        'id': 4,
        'question': [">> a = (1, 3, 5, 8, 5, 2)", ">> x = a._____(8)", ">> print(x)", ">> 3"],
        'solution': ["index"],
        'count': 0
      },
    ],
  },
  'file': {
    'close': [
      {
        'id': 1,
        'question': [">>#myFile.txt --> row1: Hello people",">> f = open('myFile.txt', 'r')", ">> print(f.read())", ">> Hello people", ">> f._____()"],
        'solution': ["close"],
        'count': 0,
      },
      {
        'id': 2,
        'question': [">>#myFile.txt --> row1: I hate banana",">> f = open('myFile.txt', 'r')", ">> print(f.read())", ">> I hate banana", ">> f._____()"],
        'solution': ["close"],
        'count': 0
      },
    ],
    'fileno': [
      {
        'id': 3,
        'question': [">>#myFile.txt --> row1: Hey Jack", ">> f = open('myFile.txt', 'r')", ">> print(f._____())", ">> 3"],
        'solution': ["fileno"],
        'count': 0,
      },
      {
        'id': 4,
        'question': [">>#myFile.txt --> row1: I have 3 dogs", ">> f = open('myFile.txt', 'r')", ">> print(f._____())", ">> 3"],
        'solution': ["fileno"],
        'count': 0
      },
    ],
    'flush': [
      {
        'id': 5,
        'question': [">>#myFile.txt --> empty", ">> f = open('myFile.txt', 'wb')",
        ">># Here it does nothing but you can call it with a read operation.", ">> f._____()", ">> f.close()"],
        'solution': ["flush"],
        'count': 0,
      },
      {
        'id': 6,
        'question': [">>#myFile.txt --> empty", ">> f = open('myFile.txt', 'w')", ">>#Here it does nothing but you can call it with a read operation.",
        ">> f._____()", ">> f.close()"],
        'solution': ["flush"],
        'count': 0
      },
    ],
    'isatty': [
      {
        'id': 7,
        'question': [">>#myFile.txt --> empty", ">> f = open('myFile.txt', 'r')", ">># check if connected on device.", ">> print(f._____())", ">> False"],
        'solution': ["isatty"],
        'count': 0,
      },
      {
        'id': 8,
        'question': [">>#myFile.txt --> empty", ">> f = open('myFile.txt', 'r')", ">># check if connected on device.", ">> print(f._____())", ">> False"],
        'solution': ["isatty"],
        'count': 0
      },
    ],
    'read': [
      {
        'id': 9,
        'question': [">>#myFile.txt --> row1: Alice; row2: kevin", ">> f = open('myFile.txt', 'r')", ">> print(f._____())", ">> Alice", ">> kevin"],
        'solution': ["read"],
        'count': 0,
      },
      {
        'id': 10,
        'question': [">>#myFile.txt --> row1: Bernard; row2: Claude", ">> f = open('myFile.txt', 'r')", ">> print(f._____(11))", ">> Bernard", ">> Cla"],
        'solution': ["read"],
        'count': 0
      },
    ],
    'readable': [
      {
        'id': 11,
        'question': [">>#myFile.txt --> row1: Banana and kiwi", ">> f = open('myFile.txt', 'r')", ">> print(f._____())", ">> True"],
        'solution': ["readable"],
        'count': 0,
      },
      {
        'id': 12,
        'question': [">>#myFile.txt --> row1: >A book and a bike", ">> f = open('myFile.txt', 'r')", ">> print(f._____())", ">> True"],
        'solution': ["readable"],
        'count': 0
      },
    ],
    'readline': [
      {
        'id': 13,
        'question': [">>#myFile.txt --> row1: Peach; row2: Apple", ">> f = open('myFile.txt', 'r')", ">> print(f._____())", ">> Peach"],
        'solution': ["readline"],
        'count': 0,
      },
      {
        'id': 14,
        'question': [">>#myFile.txt --> row1: Bus; row2: Car", ">> f = open('myFile.txt', 'r')", ">> print(f._____(1))", ">> B", ">> print(f._____(1))", ">> us"],
        'solution': ["readline"],
        'count': 0
      },
    ],
    'readlines': [
      {
        'id': 15,
        'question': [">>#myFile.txt --> row1: Julia; row2: Blandine", ">> f = open('myFile.txt', 'r')", ">> print(f._____())", ">> ['Julia\\n', 'Blandine\\n']"],
        'solution': ["readlines"],
        'count': 0,
      },
      {
        'id': 16,
        'question': [">>#myFile.txt --> row1: cat; row2: dog", ">> f = open('myFile.txt', 'r')", ">> print(f._____(3))", ">> ['cat\\n']"],
        'solution': ["readlines"],
        'count': 0
      },
    ],
    'seek': [
      {
        'id': 17,
        'question': [">>#myFile.txt --> row1: Video game", ">> f = open('myFile.txt', 'r')", ">> f._____(6)", ">> print(f.readline())", ">> game"],
        'solution': ["seek"],
        'count': 0,
      },
      {
        'id': 18,
        'question': [">>#myFile.txt --> row1: Big cat; row2: Small dog", ">> f = open('myFile.txt', 'r')", ">> f._____(9)", ">> print(f.readlines())",
        ">> [Small dog]'"],
        'solution': ["seek"],
        'count': 0
      },
    ],
    'seekable': [
      {
        'id': 19,
        'question': [">>#myFile.txt --> row1: I have 3 dogs", ">> f = open('myFile.txt', 'r')", ">> x = f._____()", ">> print(x)", ">> True"],
        'solution': ["seekable"],
        'count': 0,
      },
      {
        'id': 20,
        'question': [">>#myFile.txt --> row1: I love apple", ">> f = open('myFile.txt', 'r')", ">> x = f._____()", ">> rint(x)", ">> True"],
        'solution': ["seekable"],
        'count': 0
      },
    ],
    'tell': [
      {
        'id': 21,
        'question': [">>#myFile.txt --> row1: Big man; row2: Small man", ">> f = open('myFile.txt', 'r')", ">> f.readline()", ">> x = f._____()", ">> print(x)",
        ">> 9"],
        'solution': ["tell"],
        'count': 0,
      },
      {
        'id': 22,
        'question': [">>#myFile.txt --> row1: my phone; row2: your book", ">> f = open('myFile.txt', 'r')", ">> f.readline()", ">> x = f._____()", ">> print(x)",
        ">> 7"],
        'solution': ["tell"],
        'count': 0
      },
    ],
    'truncate': [
      {
        'id': 23,
        'question': [">>#myFile.txt --> row1: some flowers; row2: game over", ">> f = open('myFile.txt', 'a')", ">> x = f._____(4)", ">> print(x)", ">> some"],
        'solution': ["truncate"],
        'count': 0,
      },
      {
        'id': 24,
        'question': [">>#myFile.txt --> row1: a table; row2: a computer", ">> f = open('myFile.txt', 'a')", ">> x = f._____(5)", ">> print(x)", ">> a tab"],
        'solution': ["truncate"],
        'count': 0
      },
    ],
    'writable': [
      {
        'id': 25,
        'question': [">>#myFile.txt --> row1: Hey! Alex", ">> f = open('myFile.txt', 'a')", ">> x = f._____()", ">> print(x)", ">> True"],
        'solution': ["writable"],
        'count': 0,
      },
      {
        'id': 26,
        'question': [">>#myFile.txt --> row1: Over here", ">> f = open('myFile.txt', 'r')", ">> x = f._____()", ">> print(x)", ">> False"],
        'solution': ["isatty", "writable"],
        'count': 0
      },
    ],
    'write': [
      {
        'id': 27,
        'question': [">>#myFile.txt --> row1: Michel", ">> f = open('myFile.txt', 'w')", ">> f._____('hello John')", ">> f.close()", ">> f = open('myFile.txt', 'r')",
        ">> print(f.read())", ">> Hello John"],
        'solution': ["write"],
        'count': 0,
      },
      {
        'id': 28,
        'question': [">>#myFile.txt --> row1: I sleep all day", ">> f = open('myFile.txt', 'w')", ">> f._____('hello Alex')", ">> f.close()", ">> f = open('myFile.txt', 'r')",
        ">> print(f.read())", ">> hello Alex"],
        'solution': ["write"],
        'count': 0
      },
    ],
    'writelines': [
      {
        'id': 29,
        'question': [">>#myFile.txt --> row1: I eat a peach", ">> f = open('myFile.txt', 'a')", ">> f._____([' kiwi', ' banana'])", ">> f.close()",
        ">> f = open('myFile.txt', 'r')", ">> print(f.read())", ">> I eat a peach kiwi banana"],
        'solution': ["writelines"],
        'count': 0,
      },
      {
        'id': 30,
        'question': [">>#myFile.txt --> row1: Great day", ">> f = open('myFile.txt', 'w')", ">> f._____(['Alex ', 'Max '])", ">> f.close()",
        ">> f = open('myFile.txt', 'r')", ">> print(f.read())", ">> Alex Max "],
        'solution': ["writelines"],
        'count': 0
      },
    ],
  },
  'native fc': {
    'abs': [
      {
        'id': 1,
        'question': [">> a = 5.85", ">> x = _____(a)", ">> print(x)", ">> 5.85"],
        'solution': ["abs"],
        'count': 0,
      },
      {
        'id': 2,
        'question': [">> a = -7.6", ">> x = _____(a)", ">> print(x)", ">> 7.6"],
        'solution': ["abs"],
        'count': 0
      },
    ],
    'all': [
      {
        'id': 3,
        'question': [">> iter = [3, {'a', 'b'}, ()]", ">> x = _____(iter)", ">> print(x)", ">> False"],
        'solution': ["all"],
        'count': 0,
      },
      {
        'id': 4,
        'question': [">> iter = [(7, 8), {'a': yes}, 'wc]", ">> x = _____(iter)", ">> print(x)", ">> True"],
        'solution': ["all"],
        'count': 0
      },
    ],
    'any': [
      {
        'id': 5,
        'question': [">> iter = [3, 4, 0, []]", ">> x = _____(iter)", ">> print(x)", ">> True"],
        'solution': ["any"],
        'count': 0,
      },
      {
        'id': 6,
        'question': [">> iter = [[], 0, {}, ()]", ">> x = _____(iter)", ">> print(x)", ">> False"],
        'solution': ["any"],
        'count': 0
      },
    ],
    'bin': [
      {
        'id': 7,
        'question': [">> x = _____(59)", ">> print(x)", ">> 0b111011"],
        'solution': ["bin"],
        'count': 0,
      },
      {
        'id': 8,
        'question': [">> x = _____(77)", ">> print(x)", ">> 0b1001101"],
        'solution': ["bin"],
        'count': 0
      },
    ],
    'bool': [
      {
        'id': 9,
        'question': [">> x = _____([()])", ">> print(x)", ">> True"],
        'solution': ["bool"],
        'count': 0,
      },
      {
        'id': 10,
        'question': [">> x = _____({'0': 'a'})", ">> print(x)", ">> True"],
        'solution': ["bool"],
        'count': 0
      },
    ],
    'chr': [
      {
        'id': 11,
        'question': [">> x = _____(66)", ">> print(x)", ">> B"],
        'solution': ["chr"],
        'count': 0,
      },
      {
        'id': 12,
        'question': [">> x = _____(111)", ">> print(x)", ">> 0"],
        'solution': ["chr"],
        'count': 0
      },
    ],
    'complex': [
      {
        'id': 13,
        'question': [">> x = _____(3, 11)", ">> print(x)", ">> (3 + 11j)"],
        'solution': ["complex"],
        'count': 0,
      },
      {
        'id': 14,
        'question': [">> x = _____('5-8j')", ">> print(x)", ">> (5-8j)"],
        'solution': ["complex"],
        'count': 0
      },
    ],
    'delattr': [
      {
        'id': 15,
        'question': [">> class Animal:", ">>   category = 'dog'", ">>   name = 'Rox'", ">> _____(Animal, 'name')", ">> print(Animal, 'name')",
        ">> AttributeError: type object 'Animal' has no attribute 'name'."],
        'solution': ["delattr"],
        'count': 0,
      },
      {
        'id': 16,
        'question': [">> class Person:", ">>   age = 23", ">>   name = 'Théo'", ">> _____(Person, 'name')", ">> print(Person, 'name')",
        ">> AttributeError: type object 'Person' has no attribute 'name'."],
        'solution': ["delattr"],
        'count': 0
      },
    ],
    'dict': [
      {
        'id': 17,
        'question': [">> x = _____(x = 1, y = 2)", ">> print(x)", ">> {'x': 1, 'y': 2}"],
        'solution': ["dict"],
        'count': 0,
      },
      {
        'id': 18,
        'question': [">> x = _____({'name': 'Max', 'sex': 'man'}", ">> print(x)", ">> {'name': 'Max', 'sex':'man'}"],
        'solution': ["dict"],
        'count': 0
      },
    ],
    'dir': [
      {
        'id': 19,
        'question': [">> x = _____(dict)", ">> print(x)", ">> ['__class__', '__contains__', '__delattr__'....", ">> ...'popitem', 'setdefault', 'update', 'values']"],
        'solution': ["dir"],
        'count': 0,
      },
      {
        'id': 20,
        'question': [">> x = _____(str)", ">> print(x)", ">> ['__add__', '__class__', '__contains__', '__delattr__'...", ">> ...'swapcase', 'title', 'translate', 'upper', 'zfill']"],
        'solution': ["dir"],
        'count': 0
      },
    ],
    'divmod': [
      {
        'id': 21,
        'question': [">> x = _____(27, 10)", ">> print(x)", ">> (2, 7)"],
        'solution': ["divmod"],
        'count': 0,
      },
      {
        'id': 22,
        'question': [">> x = _____(363, 59)", ">> print(x)", ">> (6, 9)"],
        'solution': ["divmod"],
        'count': 0
      },
    ],
    'enumerate': [
      {
        'id': 23,
        'question': [">> fruits = {'cherry', 'apple', 'banana'}", ">> x = _____(fruits)", ">> print(list(x))",
        ">> [(0, 'cherry'), (1, 'apple'), (2, 'banana')]"],
        'solution': ["enumerate"],
        'count': 0,
      },
      {
        'id': 24,
        'question': [">> animal = {'cat': 12, 'dog': 5, 'cow': 3}", ">> x = enumerate(animal, 3)", ">> print(list(x))", ">> [(3, 'cat'), (4, 'dog'), (5, 'cow')]"],
        'solution': ["enumerate"],
        'count': 0
      },
    ],
    'filter': [
      {
        'id': 25,
        'question': [">> weight = [55, 95, 30, 125, 25]", ">> def myFunc(x):", ">>    return True if x <= 70 else False",
        ">> weight_max_trampo = _____(myFunc, weight)", ">> for x in weight_max_trampo:", ">>    print(x)", ">> 55", ">> 30", ">> 25"],
        'solution': ["filter"],
        'count': 0,
      },
      {
        'id': 26,
        'question': [">> ages = [5, 67, 35, 81, 23, 13, 98]", ">> def myFunc(x):", ">>    return True if x > 64 else False", ">> seniors = _____(myFunc, ages)",
        ">> for x in seniors:", ">>    print(x)", ">> 67", ">> 81", ">> 98"],
        'solution': ["filter"],
        'count': 0
      },
    ],
    'float': [
      {
        'id': 27,
        'question': [">> x = _____(77)", ">> print(x)", ">> 77.0"],
        'solution': ["float"],
        'count': 0,
      },
      {
        'id': 28,
        'question': [">> x = _____('69.96')", ">> print(x)", ">> 69.96"],
        'solution': ["float"],
        'count': 0
      },
    ],
    'format': [
      {
        'id': 29,
        'question': [">> x = _____(1000000, '_')", ">> print(x)", ">> 1_000_000"],
        'solution': ["format"],
        'count': 0,
      },
      {
        'id': 30,
        'question': [">> x = _____(0.75, '%')", ">> print(x)", ">> 75.000000%"],
        'solution': ["format"],
        'count': 0
      },
    ],
    'frozenset': [
      {
        'id': 31,
        'question': [">> names = ['Max', 'Julia', 'Lou']", ">> x = _____(names)", ">> print(x)", ">> _____({'Max', 'Julia', 'Lou'})"],
        'solution': ["frozenset"],
        'count': 0,
      },
      {
        'id': 32,
        'question': [">> ages = [15, 24, 7, 59]", ">> x = frozenset(ages)", ">> print(x)", ">> _____({15, 24, 59, 75})"],
        'solution': ["frozenset"],
        'count': 0
      },
    ],
    'getattr': [
      {
        'id': 33,
        'question': [">> class Vehicle:", ">>    brand = 'MW'", ">>    color = 'black'", ">> x = _____(Vehicle, 'color', 'error')", ">> print(x)", 
        ">> black"],
        'solution': ["getattr"],
        'count': 0,
      },
      {
        'id': 34,
        'question': [">> class Person:", ">>    name = 'Julia'", ">>    sex = 'woman'", ">> x = _____(Person, 'name', 'error')", ">> print(x)",
        ">> Julia"],
        'solution': ["getattr"],
        'count': 0
      },
    ],
    'globals': [
      {
        'id': 35,
        'question': [">> x = _____()", ">> print(x)", ">> {'__name__': '__main__', '__doc__': None,...", ">> ...'__builtins__': <module 'builtins' (built-in)>}"],
        'solution': ["globals"],
        'count': 0,
      },
      {
        'id': 36,
        'question': [">> y = _____()", ">> print(y)", ">> {'__name__': '__main__', '__doc__': None,...", ">> ...'__builtins__': <module 'builtins' (built-in)>}"],
        'solution': ["globals"],
        'count': 0
      },
    ],
    'hasattr': [
      {
        'id': 37,
        'question': [">> class Animal:", ">>    category = 'dog'", ">>    weight = 25", ">> x = _____(Animal, 'weight')", ">> print(x)", ">> True"],
        'solution': ["hasattr"],
        'count': 0,
      },
      {
        'id': 38,
        'question': [">> class Person:", ">>    name = 'Lucie'", ">>    age = 18", ">> x = _____(Person, 'weight')", ">> print(x)", ">> False"],
        'solution': ["hasattr"],
        'count': 0
      },
    ],
    'help': [
      {
        'id': 39,
        'question': [">> _____(str)", ">> Help on class str in module builtins:", ">> class str(object)", ">>  |  str(object='') -> str"],
        'solution': ["help"],
        'count': 0,
      },
      {
        'id': 40,
        'question': [">> _____(dict)", ">> Help on class dict in module builtins:", ">> class dict(object)", ">>  |  dict() -> new empty dictionary"],
        'solution': ["help"],
        'count': 0
      },
    ],
    'hex': [
      {
        'id': 41,
        'question': [">> x = _____(249)", ">> print(x)", ">> 0xf9"],
        'solution': ["hex"],
        'count': 0,
      },
      {
        'id': 42,
        'question': [">> x = _____(666)", ">> print(x)", ">> 0x29a"],
        'solution': ["hex"],
        'count': 0
      },
    ],
    'id': [
      {
        'id': 43,
        'question': [">> x = _____({'a': 0, 'b': 1})", ">> print(x)", ">> 2007062868096"],
        'solution': ["id"],
        'count': 0,
      },
      {
        'id': 44,
        'question': [">> x = _____((7, 8))", ">> print(x)", ">> 2007063490304"],
        'solution': ["id"],
        'count': 0
      },
    ],
    'input': [
      {
        'id': 45,
        'question': [">> age = _____('How old are you ?: ')", ">> How old are you?: 25", ">> print('You have' + age)", ">> You have 25"],
        'solution': ["input"],
        'count': 0,
      },
      {
        'id': 46,
        'question': [">> job = _____('What is your job?: ')", ">> What is your job?: dentist", ">> print('You are a ' + job)", ">> You are a dentist"],
        'solution': ["input"],
        'count': 0
      },
    ],
    'int': [
      {
        'id': 47,
        'question': [">> x = _____('54321', 6)", ">> print(x)", ">> 7465"],
        'solution': ["int"],
        'count': 0,
      },
      {
        'id': 48,
        'question': [">> x = _____('1000', 9)", ">> print(x)", ">> 729"],
        'solution': ["int"],
        'count': 0
      },
    ],
    'isinstance': [
      {
        'id': 49,
        'question': [">> x = _____(8, int)", ">> print(x)", ">> True"],
        'solution': ["isinstance"],
        'count': 0,
      },
      {
        'id': 50,
        'question': [">> class Vehicle:", ">>    color = 'red'", ">> x = Vehicle()", ">> y = _____(x, Vehicle)", ">> print(y)", ">> True"],
        'solution': ["isinstance"],
        'count': 0
      },
    ],
    'issubclass': [
      {
        'id': 51,
        'question': [">> class DogName:", ">>    name = 'Rox'", ">> class Animal(DogName):", ">>    name = DogName", ">>    category = 'dog'",
        ">> x = _____(Animal, DogName)", ">> print(x)", ">> True"],
        'solution': ["issubclass"],
        'count': 0,
      },
      {
        'id': 52,
        'question': [">> class DogName:", ">>    name = 'Rox'", ">> class Animal(DogName):", ">>    name = DogName", ">>    category = 'dog'",
        ">> x = _____(Animal, DogName)", ">> print(x)", ">> True"],
        'solution': ["issubclass"],
        'count': 0
      },
    ],
    'iter': [
      {
        'id': 53,
        'question': [">> x = _____((3, 2))", ">> print(next(x))", ">> 3", ">> print(next(x))", ">> 2"],
        'solution': ["iter"],
        'count': 0,
      },
      {
        'id': 54,
        'question': [">> x = _____({'name': 'Lola', 'age': 22})", ">> print(next(x))", ">> name", ">> print(next(x))", ">> age"],
        'solution': ["iter"],
        'count': 0
      },
    ],
    'len': [
      {
        'id': 55,
        'question': [">> a = [8, 5, 3, 4, 1, 1]", ">> x = _____(a)", ">> print(x)", ">> 6"],
        'solution': ["len"],
        'count': 0,
      },
      {
        'id': 56,
        'question': [">> name = 'Jean Baptiste'", ">> x = _____(name)", ">> print(x)", ">> 13"],
        'solution': ["len"],
        'count': 0
      },
    ],
    'list': [
      {
        'id': 57,
        'question': [">> x = _____(('a', 'b', 'c'))", ">> print(x)", ">> 6"],
        'solution': ["list"],
        'count': 0,
      },
      {
        'id': 58,
        'question': [">> x = _____({'name': 'John', 'age': 25})", ">> print(x)", ">> ['name', 'age']"],
        'solution': ["list"],
        'count': 0
      },
    ],
    'locals': [
      {
        'id': 59,
        'question': [">> x = _____()", ">> print(x)", ">> {'__name__': '__main__', '__doc__': None...", ">> ...'__builtins__': <module 'builtins' (built-in)>}"],
        'solution': ["locals"],
        'count': 0,
      },
      {
        'id': 60,
        'question': [">> y = _____()", ">> print(y)", ">> {'__name__': '__main__', '__doc__': None...", ">> ...'__builtins__': <module 'builtins' (built-in)>}"],
        'solution': ["locals"],
        'count': 0
      },
    ],
    'map': [
      {
        'id': 61,
        'question': [">> def length(a):", ">>    return len(a)", ">> x = _____(length, ['bonjour', 'hola']", ">> print(x)", ">> [7, 4]"],
        'solution': ["map"],
        'count': 0,
      },
      {
        'id': 62,
        'question': [">> def multiple(a, b):", ">>    return a*b", ">> x = _____(multiple, [3, 6, 9, 12], [2, 4, 6, 8])", ">> print(x)", ">> [6, 24, 54, 96]"],
        'solution': ["map"],
        'count': 0
      },
    ],
    'max': [
      {
        'id': 63,
        'question': [">> a = {-2: 4, -3: 9, 1: 1}", ">> key1 = _____(a)", ">> print(key1)", ">> 1",
        ">> key2 = _____(a, key=lambda k: a[k])", ">> print(key2)"],
        'solution': ["max"],
        'count': 0,
      },
      {
        'id': 64,
        'question': [">> x = _____('dog', 'cat', 'cow')", ">> print(x)", ">> dog"],
        'solution': ["max"],
        'count': 0
      },
    ],
    'min': [
      {
        'id': 65,
        'question': [">> a = {'name': 'Max', 'age': 31, 'sex': 'man'}", ">> x = _____(a)", ">> print(x)", ">> age"],
        'solution': ["min"],
        'count': 0,
      },
      {
        'id': 66,
        'question': [">> x = _____(8+3, 5-2, 3*3) ", ">> print(x)", ">> 3"],
        'solution': ["min"],
        'count': 0
      },
    ],
    'next': [
      {
        'id': 67,
        'question': [">> animal = iter(['dog', 'cat'])", ">> print(_____(animal))", ">> dog", ">> print(_____(animal))", ">> cat"],
        'solution': ["next"],
        'count': 0,
      },
      {
        'id': 68,
        'question': [">> a = iter([1, 2])", ">> print(_____(a, 'end'))", ">> 1", ">> print(_____(a, 'end'))", ">> 2", ">> print(_____(a, 'end'))", ">> end"],
        'solution': ["next"],
        'count': 0
      },
    ],
    'object': [
      {
        'id': 69,
        'question': [">> x = _____()", ">> print(x)", ">> <object object at 0x00000160AAFEFAC0>"],
        'solution': ["object"],
        'count': 0,
      },
      {
        'id': 70,
        'question': [">> y = _____()", ">> print(y)", ">> <object object at 0x00000160AAFEFA20>"],
        'solution': ["object"],
        'count': 0
      },
    ],
    'oct': [
      {
        'id': 71,
        'question': [">> x = _____(127)", ">> print(x)", ">> 0o177"],
        'solution': ["oct"],
        'count': 0,
      },
      {
        'id': 72,
        'question': [">> x = _____(-4597)", ">> print(x)", ">> -0o10765"],
        'solution': ["oct"],
        'count': 0
      },
    ],
    'open': [
      {
        'id': 73,
        'question': [">># myFile.txt row1: My name is John", ">> f = _____('myFile.txt', 'r')", ">> print(f.read())", ">> My name is John"],
        'solution': ["open"],
        'count': 0,
      },
      {
        'id': 74,
        'question': [">># myFile.txt row1: Hello Alex", ">> f = _____('myFile.txt', 'r')", ">> print(f.read())", ">> Hello Alex"],
        'solution': ["open"],
        'count': 0
      },
    ],
    'ord': [
      {
        'id': 75,
        'question': [">> x = _____('ç')", ">> print(x)", ">> 231"],
        'solution': ["ord"],
        'count': 0,
      },
      {
        'id': 76,
        'question': [">> x = _____('0')", ">> print(x)", ">> 48"],
        'solution': ["ord"],
        'count': 0
      },
    ],
    'pow': [
      {
        'id': 77,
        'question': [">> x = _____(2, 8)", ">> print(x)", ">> 256"],
        'solution': ["pow"],
        'count': 0,
      },
      {
        'id': 78,
        'question': [">> x = _____(9, 3, 50)", ">> print(x)", ">> 29"],
        'solution': ["pow"],
        'count': 0
      },
    ],
    'print': [
      {
        'id': 79,
        'question': [">> a = (1, 1)", ">> _____('tuple: ', a)", ">> tuple: (1, 1)"],
        'solution': ["print"],
        'count': 0,
      },
      {
        'id': 80,
        'question': [">> _____('a', 'b', 'c', 'd', sep='*')",">> a*b*c*d"],
        'solution': ["print"],
        'count': 0
      },
    ],
    'range': [
      {
        'id': 81,
        'question': [">> a = _____(0, 4)", ">> for i in a:", ">>    print(i)", ">> 0", ">> 1", ">> 2", ">> 3"],
        'solution': ["range"],
        'count': 0,
      },
      {
        'id': 82,
        'question': [">> for i in _____(0, 100, 22):", ">>    print(i)", ">> 0", ">> 22", ">> 44", ">> 66", ">> 88"],
        'solution': ["range"],
        'count': 0
      },
    ],
    'reversed': [
      {
        'id': 83,
        'question': [">> a = [8, 4, 2]", ">> x = _____(a)", ">> print(list(x))", ">> [2, 4, 8]"],
        'solution': ["reversed"],
        'count': 0,
      },
      {
        'id': 84,
        'question': [">> a = {'name': 'Jule', 'age': 25}", ">> x = _____(a)", ">> print(list(x))", ">> ['age', 'name']"],
        'solution': ["reversed"],
        'count': 0
      },
    ],
    'round': [
      {
        'id': 85,
        'question': [">> x = _____(7.6973, 1)", ">> print(x)", ">> 7.7"],
        'solution': ["round"],
        'count': 0,
      },
      {
        'id': 86,
        'question': [">> x = _____(4.25)", ">> print(x)", ">> 4"],
        'solution': ["round"],
        'count': 0
      },
    ],
    'set': [
      {
        'id': 87,
        'question': [">> x = _____(('hi', 'hello', 'hola'))", ">> print(x)", ">> {'hello', 'hi', 'hola'}"],
        'solution': ["set"],
        'count': 0,
      },
      {
        'id': 88,
        'question': [">> x = _____('abcdef')", ">> print(x)", ">> {'a', 'b', 'c', 'd', 'e', 'f'}"],
        'solution': ["set"],
        'count': 0
      },
    ],
    'setattr': [
      {
        'id': 89,
        'question': [">> class Vehicle:", ">>    brand = 'Peugeot'", ">>    color = 'white'", ">> _____(Vehicle, 'color, 'green')",
        ">> x = getattr(Vehicle, 'color')", ">> print(x)", ">> green"],
        'solution': ["setattr"],
        'count': 0,
      },
      {
        'id': 90,
        'question': [">> class Person:", ">>    name = 'Lola'", ">>    age = 33", ">> _____(Person, 'name', 'Pierre')",
        ">> x = getattr(Person, 'name')", ">> print(x)", ">> Pierre"],
        'solution': ["setattr"],
        'count': 0
      },
    ],
    'slice': [
      {
        'id': 91,
        'question': [">> a = '063388445577'", ">> b = _____(0, -1, 2)", ">> print(a[b])", ">> 038457"],
        'solution': ["slice"],
        'count': 0,
      },
      {
        'id': 92,
        'question': [">> a = ('hi', 'hola', 'bonjour', 'hello')", ">> b = _____(2, -1, 8)", ">> print(a[b])", ">> (bonjour)"],
        'solution': ["slice"],
        'count': 0
      },
    ],
    'sorted': [
      {
        'id': 93,
        'question': [">> a = (1, 3, 11, -5)", ">> x = _____(a)", ">> print(x)", ">> (-5, 1, 3, 11)"],
        'solution': ["sorted"],
        'count': 0,
      },
      {
        'id': 94,
        'question': [">> a = {'name': 'Jean', 'age': 20, 'sex': 'man'}", ">> x = _____(a, reverse=True)", ">> print(x)", ">> ['sex', 'name', 'age']"],
        'solution': ["sorted"],
        'count': 0
      },
    ],
    'sum': [
      {
        'id': 95,
        'question': [">> a = [-3, 1, 12, 6]", ">> x = sum(a)", ">> print(a)", ">> 16"],
        'solution': ["sum"],
        'count': 0,
      },
      {
        'id': 96,
        'question': [">> a = (6, 3, 2, 1, 3, 2)", ">> x = sum(a, -17)", ">> print(x)", ">> 0"],
        'solution': ["sum"],
        'count': 0
      },
    ],
    'super': [
      {
        'id': 97,
        'question': [">> class Parent:", ">>    def__init__(self, txt):", ">>       self.message = txt", ">>    def message(self):", 
        ">>       print(self.message)", ">> class Child(Parent):", ">>    def __init__(self, txt):", ">>       _____().__init__(txt)", 
        ">> x = Child('Hello')", ">> x.message()", ">> Hello"],
        'solution': ["super"],
        'count': 0,
      },
      {
        'id': 98,
        'question': [">> class Parent:", ">>    def__init__(self, txt):", ">>       self.message = txt", ">>    def message(self):", 
        ">>       print(self.message)", ">> class Child(Parent):", ">>    def __init__(self, txt):", ">>       _____().__init__(txt)", 
        ">> x = Child('Hello')", ">> x.message()", ">> Hello"],
        'solution': ["super"],
        'count': 0
      },
    ],
    'tuple': [
      {
        'id': 99,
        'question': [">> a = _____('hello')", ">> print(a)", ">> ('H', 'e', 'l', 'l', 'o')"],
        'solution': ["tuple"],
        'count': 0,
      },
      {
        'id': 100,
        'question': [">> a = _____((1, 2, 3, 4))", ">> print(a)", ">> (1, 2, 3, 4)"],
        'solution': ["tuple"],
        'count': 0
      },
    ],
    'type': [
      {
        'id': 101,
        'question': [">> a = [1, 2, 3]", ">> print(_____(a))", ">> <class 'list'>"],
        'solution': ["type"],
        'count': 0,
      },
      {
        'id': 102,
        'question': [">> a = {'a': 0, 'b': 1}", ">> print(_____(a))", ">> <class 'dict'>"],
        'solution': ["type"],
        'count': 0
      },
    ],
    'vars': [
      {
        'id': 103,
        'question': [">> class Vehicle:", ">>    color = 'white'", ">> x = _____(Vehicle)", ">> print(x)", ">> mappingproxy({'__module__':...",
        ">> ...of 'Vehicle' objects>, '__doc__': None})"],
        'solution': ["vars"],
        'count': 0,
      },
      {
        'id': 104,
        'question': [">> class Vehicle:", ">>    color = 'white'", ">> x = _____(Vehicle)", ">> print(x)", ">> mappingproxy({'__module__':...",
        ">> ...of 'Vehicle' objects>, '__doc__': None})"],
        'solution': ["vars"],
        'count': 0
      },
    ],
  },
}

def main():
  all_methods = []

  for i in range(0, len(OBJECTS)):
    for method in TYPE_OF[OBJECTS[i]]:
      all_methods.append(method)

  for i in range(0, len(all_methods)):
    if all_methods[i].find('('):
      x = all_methods[i].find('(')
      all_methods[i] = all_methods[i][:(x)]

  
  length = len(all_methods)

  a, b, c = 0, 0, 0
  
  for i in range(0, 100):
    a = random.randint(0, length-1)
    b = random.randint(0, length-1)

    while b == a:
      b = random.randint(0, length-1)

    c = random.randint(0, length-1)

    while c == a or c == b:
      c = random.randint(0, length-1)
      
    string1 = " {} ".format(all_methods[a])
    string2 = " {} ".format(all_methods[b])
    string3 = " {} ".format(all_methods[c])
    print(string1.ljust(30), string2.ljust(30), string3.ljust(30))




if __name__ == '__main__':
  main()


